<?php

namespace MilkMedia\GetContent;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Group extends Model implements AuditableContract
{
    use NodeTrait;
    use Auditable;

    protected $guarded = [];

    protected $casts = [
        'schema' => 'array',
        'meta' => 'array'
    ];

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
