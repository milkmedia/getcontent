<?php

namespace MilkMedia\GetContent;

use Illuminate\Support\Facades\Route;

class GetContent
{
    public static $VERSION = '0.2.6';

    /**
     * Returns the absolute class path for the route action controller
     *
     * @param $action
     *
     * @return string
     */
    public static function actionPath($action): string
    {
        return "\MilkMedia\GetContent\Http\Controllers\\{$action}";
    }

    /**
     * Provider GetContent API routes
     */
    public static function apiRoutes(): void
    {
        Route::pattern('directory', '(.+)');

        Route::get('/users/me', self::actionPath('Api\UsersController@authorised'));

        Route::get('/documents', self::actionPath('Api\DocumentController@index'));
        Route::post('/documents', self::actionPath('Api\DocumentController@store'));
        Route::get('/documents/search', self::actionPath('Api\DocumentSearchController@index'));
        Route::get('/documents/{document}', self::actionPath('Api\DocumentController@show'));
        Route::put('/documents/{document}', self::actionPath('Api\DocumentController@update'));
        Route::delete('/documents/{document}', self::actionPath('Api\DocumentController@destroy'));

        Route::get('/groups', self::actionPath('Api\GroupController@index'));
        Route::get('/groups/tree', self::actionPath('Api\GroupTreeController@index'));
        Route::get('/groups/{group}', self::actionPath('Api\GroupController@show'));
        Route::post('/groups', self::actionPath('Api\GroupController@store'));
        Route::put('/groups/{group}', self::actionPath('Api\GroupController@update'));
        Route::delete('/groups/{group}', self::actionPath('Api\GroupController@destroy'));

        Route::get('/groups/{group}/documents', self::actionPath('Api\GroupDocumentsController@index'));

        Route::get('/files/{directory?}', self::actionPath('Api\FileController@index'));
        Route::delete('/files/{directory?}', self::actionPath('Api\FileController@destroy'));
        Route::post('/files/{directory?}', self::actionPath('Api\FileController@store'));

        Route::get('/templates', self::actionPath('Api\TemplateController@index'));
        Route::get('/templates/{template}', self::actionPath('Api\TemplateController@show'));
        Route::post('/templates', self::actionPath('Api\TemplateController@store'));
        Route::put('/templates/{template}', self::actionPath('Api\TemplateController@update'));
        Route::delete('/templates/{template}', self::actionPath('Api\TemplateController@destroy'));
    }

    /**
     * Provide routes for the GetContent editor
     */
    public static function editorRoutes(): void
    {
        Route::view('editor', 'GetContent::editor.index');
    }

    /**
     * Provider route to display Documents
     *
     * @param string $slugExpression Set the RegEx used to identify the document slug
     */
    public static function webRoutes($slugExpression = '(.*)'): void
    {
        Route::get('/_image/{path}', self::actionPath('ImageController@show'))
            ->where('path', '.*');

        Route::get('/{slug?}', self::actionPath('DocumentController@show'))
            ->where('slug', $slugExpression);
    }

    /**
     * Generates a look up Collection of potential
     * Views based on the URL segments
     *
     * @param      $segments
     *
     * @param null $document
     *
     * @param bool $includeDefaultViews
     *
     * @return \Illuminate\Support\Collection
     */
    public static function viewsFromSegments(
        $segments,
        $document = null,
        $includeDefaultViews = true
    ): \Illuminate\Support\Collection {
        return tap(
            collect($segments)
                ->map(function ($segment, $key) use ($segments) {

                    $trail = collect($segments)->take($key)->push($segment)->implode('.');

                    return ["$trail.index", $trail];
                })->collapse()->reverse(),
            function ($views) use ($includeDefaultViews, $document) {
                if ($document) {
                    $views->push('document');
                    if ($includeDefaultViews) {
                        $views->push('GetContent::document');
                    }
                }
            }
        );
    }

    /**
     * Returns all Documents in the specified Group
     *
     * @param $group
     *
     * @return mixed
     */
    public static function documentsInGroup($group)
    {
        return Group::whereSlug($group)->firstOrFail()->documents();
    }

    /**
     * Get a document by Slug scoped to descendants of a specific Group
     *
     * @param $groupSlug
     * @param $documentSlug
     *
     * @return mixed
     */
    public static function documentBySlugWithinGroup($documentSlug, $groupSlug)
    {
        $group = Group::whereSlug($groupSlug)->firstOrFail()->id;

        return Document::whereSlug($documentSlug)->withinGroup($group)->firstOrFail();
    }

    /**
     * Get the configured and permitted storage path
     *
     * @param null $directory
     * @return string
     */
    static function filePath($directory = null)
    {
        $path = [config('getcontent.storage_path')];

        if ($rootDirectory = auth()->user()->getSetting('permissions.filesRootDirectory')) {
            $path[] = $rootDirectory;
        }

        $path[] = $directory;
        $path = implode('/', $path);

        // Block jumping up directories
        $path = str_replace(['../', '/..', '..'], '/', $path);

        return trim($path, '/');
    }
}
