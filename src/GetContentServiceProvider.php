<?php

namespace MilkMedia\GetContent;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use MilkMedia\GetContent\Observers\DocumentObserver;
use MilkMedia\GetContent\Observers\GroupObserver;
use MilkMedia\GetContent\Observers\TemplateObserver;
use MilkMedia\GetContent\Policies\DocumentPolicy;
use MilkMedia\GetContent\Policies\GroupPolicy;

class GetContentServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/getcontent.php',
            'getcontent'
        );

        $this->app->singleton(GetContent::class, function ($app) {
            return new GetContent(config('getconfig'));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // The policy mappings for the application.
        Gate::policy(Group::class, GroupPolicy::class);
        Gate::policy(Document::class, DocumentPolicy::class);

        $this->publishes([
            __DIR__ . '/../config/getcontent.php' => config_path('getcontent.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../public' => public_path('vendor/milkmedia/getcontent'),
        ], 'public');

        if(config('app.env') !== 'testing') {
            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        }

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'GetContent');

        View::share('GetContent', new GetContent());
        Document::observe(DocumentObserver::class);
        Group::observe(GroupObserver::class);
        Template::observe(TemplateObserver::class);

        Route::bind('document', function ($value) {
            $documents = Document::withoutGlobalScopes(['approved', 'published']);

            if (is_numeric($value)) {
                $documents->where('id', $value);
            } else {
                $documents->where('uuid', $value);
            }

            return $documents->first() ?? abort(404);
        });

        Route::bind('group', function ($value) {
            $group = Group::query();

            if (is_numeric($value)) {
                $group->where('id', $value);
            } else {
                $group->where('uuid', $value);
            }

            return $group->first() ?? abort(404);
        });
    }
}
