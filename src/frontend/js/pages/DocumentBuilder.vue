<template>
  <div class="inspector-grid h-screen bg-white" v-if="document">
    <nav
      class="grid-header flex border-b items-center justify-between p-4"
      :class="{ 'opacity-50 pointer-events-none': loading }"
    >
      <div class="w-full">
        <text-field
          class="text-2xl mt-1"
          v-model="document.name"
          :field="{ placeholder: 'Give the document a name' }"
          @click="unselectField()"
        />
      </div>

      <section class="flex-no-shrink text-sm leading-none">
        <input-button
          v-if="!schemaLocked"
          :type="arrangeFields ? 'solid' : 'text'"
          @click="arrangeFields = !arrangeFields"
        >
          <svg-icon
            name="bold/arrange"
            class="fill w-4 ml-1 align-middle"
          ></svg-icon>
          Arrange fields
        </input-button>
        <input-button type="text" color="grey" @click="close"
          >Close
        </input-button>
        <input-button @click="preview" :disabled="!document.slug"
          >Preview
        </input-button>
        <input-button
          type="solid"
          color="green"
          class="relative px-4 font-bold"
          @click="save"
        >
          Save {{ document.status === "draft" ? "Draft" : null }}
          <span
            v-if="dirty"
            class="absolute block pin-t pin-r w-3 h-3 -mt-1 -mr-1 rounded-full bg-orange border border-white animated bounce"
          ></span>
        </input-button>
      </section>
    </nav>

    <main class="flex items-center" v-if="loading">
      <ui-loader></ui-loader>
    </main>

    <main v-else class="w-full mt-1 overflow-auto" @click.self="unselectField">
      <div class="p-6 max-w-xl mx-auto" v-if="arrangeFields && document.schema">
        <ul class="list-reset">
          <draggable v-model="document.schema" :options="{ animation: 100 }">
            <transition-group>
              <component-preview
                v-for="field in document.schema"
                :key="field.model"
                :field="field"
                class="bg-grey-lightest rounded p-3 my-3 cursor-move"
              ></component-preview>
            </transition-group>
          </draggable>
        </ul>
      </div>

      <article class="p-6 max-w-xl mx-auto" v-else-if="model">
        <component-container
          v-for="(component, id) in schema"
          :key="component.model"
          :component="component"
          :scope.sync="document.model[component.model]"
          @remove="removeField(id)"
          @click="selectField(component)"
          class="field-component my-6 p-1"
          :class="{ 'selected-field': selectedComponent === component }"
          :selected="selectedComponent === component"
        ></component-container>

        <footer v-if="!schemaLocked">
          <add-fields @add-field="addField"></add-fields>
        </footer>
      </article>
    </main>

    <aside class="flex" :class="{ 'opacity-50 pointer-events-none': loading }">
      <document-builder-inspector
        :document="document"
        :selected-tab.sync="inspectorTab"
        :selected-component="selectedComponent"
        @open-devtools="devToolsOpen = true"
      ></document-builder-inspector>
    </aside>

    <ui-modal :is-open.sync="devToolsOpen" :close-delay="300">
      <transition name="slide" appear slot-scope="{ isOpen, close }">
        <section
          class="flex flex-col bg-white p-4 fixed pin-r pin-t pin-b w-3/4"
          v-if="isOpen"
        >
          <header class="flex justify-between items-center">
            <span class="flex items-center text-grey-darker">
              <svg-icon
                name="bold/programming-code"
                class="w-6 h-6 mr-3 fill text-grey-dark"
              ></svg-icon>
              Developer Tools
            </span>
            <input-button type="text" color="grey" @click="close">
              <svg-icon name="bold/close" class="fill w-4"></svg-icon>
            </input-button>
          </header>
          <developer-tools :document.sync="document"></developer-tools>
        </section>
      </transition>
    </ui-modal>

    <ui-dialog
      title="Unsaved changes will be lost"
      icon="bold/alert-triangle"
      color="orange"
      :is-open.sync="confirmLeave"
    >
      <p>You have unsaved changes that will be lost.</p>
      <template slot="footer" slot-scope="{ close }">
        <input-button type="text" color="orange" @click="continueLeaving()"
          >Ok, discard changes
        </input-button>
        <input-button type="solid" @click="close">No, go back</input-button>
      </template>
    </ui-dialog>
  </div>
</template>

<script>
import {
  cloneDeep,
  isEqualWith,
  isEmpty,
  each,
  has,
  get,
  isArray,
  assign
} from "lodash";
import draggable from "vuedraggable";
import ComponentContainer from "../components/ComponentContainer";
import ComponentPreview from "../components/ComponentPreview";
import DocumentBuilderInspector from "../components/DocumentBuilderInspector";
import AddFields from "../components/AddFields";
import Group from "../models/Group";
import Document from "../models/Document";
import DeveloperTools from "../components/DeveloperTools";
import TextField from "../components/fields/TextFieldComponent/index";
import ComponentField from "../components/ComponentField";
import DocumentIcon from "../components/DocumentIcon";

export default {
  components: {
    DocumentIcon,
    ComponentField,
    TextField,
    DeveloperTools,
    draggable,
    ComponentContainer,
    ComponentPreview,
    DocumentBuilderInspector,
    AddFields
  },

  provide: function() {
    return {
      document: () => this.document,
      schemaLocked: () => this.schemaLocked
    };
  },

  props: {
    uuid: {
      default: null
    },

    groupUuid: {
      default: null
    }
  },

  data: () => ({
    groupReady: false,
    documentReady: false,
    confirmLeave: false,
    leavingTo: null,
    document: null,
    originalDocument: null,
    loadedGroup: null,
    showFields: false,
    arrangeFields: false,
    selectedComponent: null,
    inspectorTab: "document",
    typeCount: {},
    devToolsOpen: false
  }),

  computed: {
    loading() {
      return !this.documentReady || !this.groupReady;
    },

    /**
     * The group the document belongs to
     */
    group() {
      if (this.document && this.document.group) {
        return new Group(this.document.group);
      }

      return this.loadedGroup || null;
    },

    /**
     * Check for any changes to the document
     */
    dirty() {
      if (!this.originalDocument) {
        return true;
      }

      return !isEqualWith(
        this.document,
        this.originalDocument,
        (value, othValue) => {
          // Allow empty strings and null to be equal
          if (
            (value === "" && othValue === null) ||
            (value === null && othValue === "") ||
            (isEmpty(value) && isEmpty(othValue))
          ) {
            return true;
          }
        }
      );
    },

    model() {
      return this.document.model;
    },

    schema() {
      if (this.document.schema && this.document.schema.length > 0) {
        return this.document.schema;
      }

      return get(this.group, "schema", []);
    },

    groupHasSchema() {
      return get(this.group, "schema", []).length;
    },

    schemaLocked() {
      return (
        (this.document && this.document.getMetaOption("schemaLocked")) ||
        (this.group && this.group.getMetaOption("schemaLocked"))
      );
    }
  },

  created() {
    this.loadDocument();
    if (this.groupUuid) {
      Group.find(this.groupUuid).then(group => {
        this.loadedGroup = new Group(group.data);
        this.groupReady = true;
      });
    } else {
      this.groupReady = true;
    }
  },

  methods: {
    /**
     * Load a document from the store, or create a new blank document
     */
    loadDocument(uuid = this.uuid) {
      if (uuid) {
        Document.include("documents", "group")
          .include("documents")
          .find(uuid)
          .then(payload => {
            this.originalDocument = new Document(payload.data);
            this.document = new Document(cloneDeep(payload.data));

            setTimeout(() => {
              this.documentReady = true;
            }, 500);
          });
      } else {
        this.document = new Document({ model: {}, schema: [], meta: [] });

        setTimeout(() => {
          this.documentReady = true;
        }, 500);
      }
    },

    initModel() {
      // Copy group schema if it exists
      // and group is not locked
      if (
        this.groupHasSchema &&
        !this.group.getMetaOption("schemaLocked") &&
        !this.document.schema.length
      ) {
        this.document.schema = this.group.schema;
      }

      // Init model keys from the schema if
      // they don't exist in the model
      each(this.schema, component => {
        if (!has(this.model, component.model)) {
          this.$set(this.model, component.model, { value: null });
        }
      });
    },

    /**
     * Keep track of types of field to generate variable names
     */
    nextOfType(type) {
      has(this.typeCount, type)
        ? this.typeCount[type]++
        : (this.typeCount[type] = 1);

      let nextVariable = `${type}${this.typeCount[type]}`;

      return has(this.model, nextVariable)
        ? this.nextOfType(type)
        : nextVariable;
    },

    /**
     * Adds a field to the document
     *
     * @param type
     * @param template
     */
    addField(type, template = null) {
      let newField = {
        type: type,
        model: this.nextOfType(type)
      };

      if (template) {
        newField.template = template.slug;
      }

      this.$set(this.document.model, newField.model, { options: {} });
      this.document.schema.push(newField);
    },

    /**
     * Remove a field from the document
     *
     * @param fieldIndex
     */
    removeField(fieldIndex) {
      this.$delete(this.model, this.document.schema[fieldIndex].model);
      this.document.schema.splice(fieldIndex, 1);
      this.$nextTick(() => {
        this.selectedComponent = null;
      });
    },

    /**
     * Open a preview link in a new window
     */
    preview() {
      window.open(`${this.document.slug}?preview`);
    },

    /**
     * Close the document
     */
    close() {
      if (this.group) {
        return this.$router.push({
          name: "BrowseGroup",
          params: { groupUuid: this.group.uuid }
        });
      }

      return this.$router.push({ name: "BrowseDocuments" });
    },

    /**
     * Save changes to the document
     */
    save() {
      if (!this.document.group && !this.document.group_id && this.loadedGroup) {
        this.document.group_id = this.loadedGroup.id;
      }

      this.$store.dispatch("Documents/saveDocument", this.document).then(
        document => {
          this.$notify({
            group: "general",
            type: "success",
            title: "Document saved"
          });
          this.loadDocument(document.uuid);
        },
        () => {
          this.$notify({
            group: "general",
            type: "warning",
            title: "Failed to save document"
          });
        }
      );
    },

    /**
     * Selects a field
     *
     * @param field
     */
    selectField(field) {
      this.selectedComponent = field;
      this.inspectorTab = "field";
    },

    unselectField() {
      this.inspectorTab = "document";
      this.selectedComponent = null;
    },

    /**
     * Block leaving the page if there are unsaved changes
     */
    blockLeave(to, from, next) {
      if (!this.dirty || this.confirmLeave) {
        return next();
      }

      this.leavingTo = to;
      this.confirmLeave = true;
      // TODO: seems to be causing an error when triggered by programmatic router navigation in close function
      // next(false);
    },

    /**
     * Continue leaving if confirmed
     */
    continueLeaving() {
      this.$router.push({
        name: this.leavingTo.name,
        params: this.leavingTo.params
      });
    }
  },

  watch: {
    originalDocument() {
      if (
        this.document.model &&
        isArray(this.document.model) &&
        !this.document.model.length
      ) {
        this.document.model = {};
      }

      // Upgrade to new schema/model format
      if (
        this.document &&
        this.document.content &&
        this.document.content.fields
      ) {
        this.document.upgradeModelSchema();
        this.$notify({
          group: "general",
          title: "Document format updated",
          text: "This document has been updated to the new format"
        });
      }

      this.initModel();
    },

    loading(stillLoading) {
      if (!stillLoading) {
        this.initModel();
      }
    },

    dirty(dirty) {
      this.$emit("update:dirty", dirty);
    }
  },

  beforeRouteLeave(to, from, next) {
    this.blockLeave(to, from, next);
  }
};
</script>
