/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'fields/more': {
    width: 16,
    height: 16,
    viewBox: '0 0 50 50',
    data: '<path pid="0" d="M9 19c-3.309 0-6 2.691-6 6s2.691 6 6 6 6-2.691 6-6-2.691-6-6-6zm16 0c-3.309 0-6 2.691-6 6s2.691 6 6 6 6-2.691 6-6-2.691-6-6-6zm16 0c-3.309 0-6 2.691-6 6s2.691 6 6 6 6-2.691 6-6-2.691-6-6-6z"/>'
  }
})
