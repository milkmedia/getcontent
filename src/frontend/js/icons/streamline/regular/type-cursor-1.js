/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'streamline/regular/type-cursor-1': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<g stroke-linecap="round" stroke-width="1.5" _stroke="#000" _fill="none" stroke-linejoin="round"><path pid="0" d="M2.25 17.248a1.5 1.5 0 01-1.5-1.5v-7.5a1.5 1.5 0 011.5-1.5h19.5a1.5 1.5 0 011.5 1.5v7.5a1.5 1.5 0 01-1.5 1.5zM3.75 9.75v4.5"/></g>'
  }
})
