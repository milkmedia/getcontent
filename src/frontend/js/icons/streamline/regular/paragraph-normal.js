/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'streamline/regular/paragraph-normal': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<g stroke-linecap="round" stroke-width="1.5" _stroke="#000" _fill="none" stroke-linejoin="round"><path pid="0" d="M2.25 23.248a1.5 1.5 0 01-1.5-1.5v-19.5a1.5 1.5 0 011.5-1.5h19.5a1.5 1.5 0 011.5 1.5v19.5a1.5 1.5 0 01-1.5 1.5zM9.75 5.25h9M5.25 14.25h13.5M5.25 9.75h13.5M5.25 18.75h7.5"/></g>'
  }
})
