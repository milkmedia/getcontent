/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'streamline/regular/common-file-double-1': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<g stroke-linecap="round" stroke-width="1.5" _stroke="#000" _fill="none" stroke-linejoin="round"><path pid="0" d="M17.25 23.25H3.75h0a1.5 1.5 0 01-1.5-1.5s0 0 0 0V5.25"/><path pid="1" d="M6.25 20.25a1 1 0 01-1-1V1.75a1 1 0 011-1h14.5a1 1 0 011 1v17.5a1 1 0 01-1 1z"/></g>'
  }
})
