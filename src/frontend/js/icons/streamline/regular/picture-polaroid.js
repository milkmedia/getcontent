/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'streamline/regular/picture-polaroid': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<g stroke-linecap="round" stroke-width="1.5" _stroke="#000" _fill="none" stroke-linejoin="round"><path pid="0" d="M2.25 18a1.5 1.5 0 01-1.5-1.5V2.25a1.5 1.5 0 011.5-1.5H16.5a1.5 1.5 0 011.5 1.5V16.5a1.5 1.5 0 01-1.5 1.5zM.75 13.5H18"/><path pid="1" d="M7.612 21.205l10.524 2.008h0a2.046 2.046 0 002.393-1.625l2.684-14.064h0a2.046 2.046 0 00-1.625-2.393l-.383-.073"/></g>'
  }
})
