/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'streamline/regular/module': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<g stroke-linecap="round" stroke-width="1.5" _stroke="#000" _fill="none" stroke-linejoin="round"><path pid="0" d="M1.817 4.524l9.1-3.568h0a2.936 2.936 0 012.162 0l9.1 3.568h0a1.763 1.763 0 011.071 1.662v11.628h0a1.763 1.763 0 01-1.067 1.662l-9.1 3.569h0a2.956 2.956 0 01-2.162 0l-9.1-3.569h0A1.763 1.763 0 01.75 17.814V6.186h0a1.763 1.763 0 011.067-1.662zM12 9.35L1.1 5.08M12 9.35l10.9-4.27M12 23.25V9.35"/></g>'
  }
})
