/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'bold/fields': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<circle pid="0" cx="5" cy="5" r="5"/><circle pid="1" cx="19" cy="5" r="5"/><circle pid="2" cx="5" cy="19" r="5"/><circle pid="3" cx="19" cy="19" r="5"/>'
  }
})
