/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'bold/reorder': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<rect pid="0" x="6" y="3" width="18" height="4" rx="1.5" ry="1.5"/><rect pid="1" x="3" y="10" width="18" height="4" rx="1.5" ry="1.5"/><rect pid="2" y="17" width="18" height="4" rx="1.5" ry="1.5"/>'
  }
})
