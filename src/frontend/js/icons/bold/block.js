/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'bold/block': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<rect pid="0" x=".5" y=".997" width="10" height="10" rx="2" ry="2"/><rect pid="1" x=".5" y="12.997" width="10" height="10" rx="2" ry="2"/><path pid="2" d="M13.747 3h9a.75.75 0 000-1.5h-9a.75.75 0 000 1.5zm9.003 1.5h-9a.75.75 0 000 1.5h9a.75.75 0 000-1.5zm0 3h-9a.75.75 0 000 1.5h9a.75.75 0 000-1.5zm0 7h-9a.75.75 0 000 1.5h9a.75.75 0 000-1.5zm0 3h-9a.75.75 0 000 1.5h9a.75.75 0 000-1.5zm0 3h-9a.75.75 0 000 1.5h9a.75.75 0 000-1.5z"/>'
  }
})
