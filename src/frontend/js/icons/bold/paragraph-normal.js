/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'bold/paragraph-normal': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M22.044 0H1.956A1.959 1.959 0 000 1.954v20.088A1.959 1.959 0 001.956 24h20.088A1.959 1.959 0 0024 22.042V1.954A1.959 1.959 0 0022.044 0zM21 22l-18 .04a1 1 0 01-1-1L1.958 3a1 1 0 011-1L21 1.956a1 1 0 011 1V21a1 1 0 01-1 1z"/><path pid="1" d="M4.5 8.5h15a1 1 0 000-2h-15a1 1 0 100 2zm0 5h15a1 1 0 000-2h-15a1 1 0 100 2zm0 5h6a1 1 0 000-2h-6a1 1 0 000 2z"/>'
  }
})
