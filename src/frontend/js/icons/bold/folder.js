/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'bold/folder': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M20 6.5h-9.5a.5.5 0 01-.359-.153L8.8 4.959A1.506 1.506 0 007.718 4.5H4.286c-.986.001-1.784.8-1.786 1.785v11.43c.002.985.8 1.784 1.786 1.785h15.428a1.788 1.788 0 001.786-1.785V8A1.5 1.5 0 0020 6.5z"/>'
  }
})
