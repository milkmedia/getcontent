/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'light/media': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<g stroke-linecap="round" _fill="none" stroke-linejoin="round"><path pid="0" d="M3 19a2 2 0 01-2-2V3a2 2 0 012-2h14a2 2 0 012 2v14a2 2 0 01-2 2zM1 15h18"/><path pid="1" d="M7.71 21L18 22.964a2 2 0 002.339-1.589l2.625-13.752a2 2 0 00-1.589-2.34L21 5.212"/></g>'
  }
})
