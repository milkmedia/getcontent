/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'light/folder-upload': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M20.5 8.5v-4a1 1 0 00-1-1H9.618a1 1 0 01-.894-.553l-.948-1.894A1 1 0 006.882.5H1.5a1 1 0 00-1 1v15a1 1 0 001 1h7" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round"/><circle pid="1" cx="17.5" cy="17.5" r="6" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round"/><path pid="2" d="M17.5 20.5v-6m0 0l-2.25 2.25m2.25-2.25l2.25 2.25" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round"/>'
  }
})
