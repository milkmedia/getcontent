/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'light/documents': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M7 2h9.586a1 1 0 01.707.293l2.414 2.414a1 1 0 01.293.707V19a1 1 0 01-1 1H7a1 1 0 01-1-1V3a1 1 0 011-1z" _fill="none" _stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/><path pid="1" d="M17 22H5a1 1 0 01-1-1V5" _fill="none" _stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>'
  }
})
