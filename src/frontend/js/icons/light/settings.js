/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'light/settings': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<rect pid="0" class="a" x=".5" y=".5" width="23" height="23" rx="1" ry="1"/><path pid="1" class="a" d="M12 13V4.5m0 15V17"/><circle pid="2" class="a" cx="12" cy="15" r="2"/><path pid="3" class="a" d="M6.5 12v7.5m0-15V8"/><circle pid="4" class="a" cx="6.5" cy="10" r="2"/><path pid="5" class="a" d="M17.5 12v7.5m0-15V8"/><circle pid="6" class="a" cx="17.5" cy="10" r="2"/>'
  }
})
