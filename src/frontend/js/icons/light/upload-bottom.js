/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'light/upload-bottom': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<g stroke-linecap="round" _stroke="#000" _fill="none" stroke-linejoin="round"><path pid="0" d="M23 18.218v1.913h0A2.87 2.87 0 0120.131 23H3.869h0A2.869 2.869 0 011 20.131v-1.913M12 1v17.11M5 8l7-7 7 7"/></g>'
  }
})
