/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'light/layouts': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M10.5 9.5a1 1 0 01-1 1h-8a1 1 0 01-1-1v-8a1 1 0 011-1h8a1 1 0 011 1zm13 0a1 1 0 01-1 1h-8a1 1 0 01-1-1v-8a1 1 0 011-1h8a1 1 0 011 1zm-13 13a1 1 0 01-1 1h-8a1 1 0 01-1-1v-8a1 1 0 011-1h8a1 1 0 011 1zm13 0a1 1 0 01-1 1h-8a1 1 0 01-1-1v-8a1 1 0 011-1h8a1 1 0 011 1z" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round"/>'
  }
})
