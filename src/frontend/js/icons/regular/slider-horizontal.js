/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'regular/slider-horizontal': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M9 16.25a3.75 3.75 0 113.75-3.75A3.75 3.75 0 019 16.25zM.751 12.5h1.5m13.5 0h7.5" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>'
  }
})
