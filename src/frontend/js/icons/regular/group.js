/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'regular/group': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M23.255 7.5V2.247a1.5 1.5 0 00-1.5-1.5h-5.25m0 22.5h5.25a1.5 1.5 0 001.5-1.5V16.5m-22.5 0v5.25a1.5 1.5 0 001.5 1.5h5.25m0-22.503h-5.25a1.5 1.5 0 00-1.5 1.5V7.5" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>'
  }
})
