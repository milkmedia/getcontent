/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'regular/calendar': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<rect pid="0" x=".75" y="3.75" width="22.5" height="19.5" rx="1.5" ry="1.5" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/><path pid="1" d="M.75 9.75h22.5M6.75 6V.75M17.25 6V.75M5.625 13.5a.375.375 0 10.375.375.375.375 0 00-.375-.375m0 5.25a.375.375 0 10.375.375.375.375 0 00-.375-.375M12 13.5a.375.375 0 10.375.375A.375.375 0 0012 13.5m0 5.25a.375.375 0 10.375.375.375.375 0 00-.375-.375m6.375-5.25a.375.375 0 10.375.375.375.375 0 00-.375-.375m0 5.25a.375.375 0 10.375.375.375.375 0 00-.375-.375" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>'
  }
})
