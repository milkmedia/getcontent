/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'regular/bin': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M1.751 6.368l20.541-4.366M13.6.783l-4.4.935A1.5 1.5 0 008.042 3.5l.312 1.468 7.336-1.56-.312-1.467A1.5 1.5 0 0013.6.783zM10.751 18v-7.5m4.5 7.5v-7.5M18.626 6h2.625l-1.385 15.874a1.5 1.5 0 01-1.5 1.376H7.631a1.5 1.5 0 01-1.494-1.376L5.1 9.377" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>'
  }
})
