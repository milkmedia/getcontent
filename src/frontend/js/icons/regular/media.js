/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'regular/media': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<rect pid="0" x=".75" y=".75" width="17.25" height="17.25" rx="1.5" ry="1.5" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/><path pid="1" d="M.75 13.5H18M7.612 21.205l10.524 2.008a2.046 2.046 0 002.393-1.625l2.684-14.064a2.046 2.046 0 00-1.625-2.393l-.383-.073" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>'
  }
})
