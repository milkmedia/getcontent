/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'regular/typing': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<rect pid="0" x=".75" y="5.249" width="22.5" height="13.5" rx="1.5" ry="1.5" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/><path pid="1" d="M6.75 15.749v-7.5m-1.5 0h3m-3 7.5h3" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>'
  }
})
