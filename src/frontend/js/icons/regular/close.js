/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'regular/close': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M.75 23.249l22.5-22.5m0 22.5L.75.749" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>'
  }
})
