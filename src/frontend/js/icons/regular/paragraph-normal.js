/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'regular/paragraph-normal': {
    width: 16,
    height: 16,
    viewBox: '0 0 24 24',
    data: '<rect pid="0" x=".75" y=".748" width="22.5" height="22.5" rx="1.5" ry="1.5" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/><path pid="1" d="M9.75 5.248h9m-13.5 9h13.5m-13.5-4.5h13.5m-13.5 9h7.5" _fill="none" _stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>'
  }
})
