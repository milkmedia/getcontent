import DocumentBrowser from "./pages/DocumentBrowser";
import EditDocument from "./pages/EditDocument";
import DocumentBuilder from "./pages/DocumentBuilder";
import GroupBuilder from "./pages/GroupBuilder";
import FileBrowser from "./pages/FileBrowser";
import Settings from "./pages/Settings";
import DocumentList from "./components/DocumentList";

export default [
  {
    path: '/document-list',
    component: DocumentList
  },
  {
    name: "Home",
    path: "/",
    redirect: "/documents"
  },
  {
    name: "BrowseDocuments",
    path: "/documents",
    component: DocumentBrowser
  },
  {
    name: "NewDocument",
    path: "/documents/new/:groupUuid?",
    component: DocumentBuilder,
    props: true
  },
  {
    name: "EditDocument",
    path: "/documents/edit/:uuid",
    component: DocumentBuilder,
    props: true
  },
  {
    name: "BrowseGroup",
    path: "/documents/:groupUuid",
    component: DocumentBrowser,
    props: true
  },
  {
    name: "NewGroup",
    path: "/groups/new/:groupUuid?",
    component: GroupBuilder,
    props: true
  },
  {
    name: "EditGroup",
    path: "/groups/edit/:uuid",
    component: GroupBuilder,
    props: true
  },
  {
    name: "BrowseFiles",
    path: "/files/:path?",
    component: FileBrowser,
    props: true
  },
  {
    name: "Settings",
    path: "/settings",
    component: Settings
  }
];
