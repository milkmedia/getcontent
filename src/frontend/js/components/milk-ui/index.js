import InputText from "./InputText";
import InputNumber from "./InputNumber";
import InputSwitch from "./InputSwitch";
import InputButton from "./InputButton";
import InputSelect from "./InputSelect";
import InputDate from "./InputDate";
import UiModal from "./UiModal";
import UiDialog from "./UiDialog";
import UiPopover from "./UiPopover";
import UiMenu from "./UiMenu";
import UiMenuItem from "./UiMenuItem";
import UiLoader from "./UiLoader";

export default {
  install(Vue) {
    Vue.component("input-text", InputText);
    Vue.component("input-number", InputNumber);
    Vue.component("input-switch", InputSwitch);
    Vue.component("input-button", InputButton);
    Vue.component("input-select", InputSelect);
    Vue.component("input-date", InputDate);
    Vue.component("ui-modal", UiModal);
    Vue.component("ui-dialog", UiDialog);
    Vue.component("ui-popover", UiPopover);
    Vue.component("ui-menu", UiMenu);
    Vue.component("ui-menu-item", UiMenuItem);
    Vue.component("ui-loader", UiLoader);
  }
};
