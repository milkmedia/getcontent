import {
  map,
  trimStart,
  includes,
  reject,
  forEach,
  tap,
  upperFirst
} from "lodash";

export default {
  /**
   * Convert a type to a Field Component name
   *
   * @param type
   * @returns {string}
   */
  typeToComponentName(type) {
    return `${upperFirst(type)}FieldComponent`;
  },

  /**
   * Gets a list of available components
   *
   * @returns {*}
   */
  getFields() {
    let index = require.context("./", true, /FieldComponent$/);

    return reject(
      map(index.keys(), field => {
        return trimStart(field, "./");
      }),
      field => field === "UnknownFieldComponent"
    );
  },

  /**
   * Imports a field component or returns the Unknown component
   *
   * @param componentName
   * @returns {*}
   */
  fieldComponent(componentName) {
    if (!includes(this.getFields(), componentName)) {
      return require("./UnknownFieldComponent");
    }

    return require(/* webpackChunkName: "documentFields." */
    `./${componentName}`);
  },

  /**
   * Get the Field Component from a type identifier
   *
   * @param type
   * @returns {*}
   */
  fieldComponentFromType(type) {
    return this.fieldComponent(this.typeToComponentName(type)).default;
  },

  /**
   * Imports all field components
   *
   * @returns {*}
   */
  fieldComponents() {
    return tap({}, componentsArray => {
      forEach(this.getFields(), field => {
        componentsArray[field] = this.fieldComponent(field);
      });
    });
  },

  /**
   * Imports the Field Inspector component
   *
   * @param componentName
   * @returns {*}
   */
  fieldInspector(componentName) {
    return require(/* webpackChunkName: "documentFieldInspectors." */
    `./${componentName}/inspector`);
  },

  /**
   * Imports the Field Builder component
   *
   * @param componentName
   * @returns {*}
   */
  fieldBuilder(componentName) {
    return require(/* webpackChunkName: "documentFieldBuilders." */
    `./${componentName}/builder`);
  },

  /**
   * Imports the Field Builder component by the type identifier
   *
   * @param type
   * @returns {*}
   */
  fieldBuilderByType(type) {
    return require(/* webpackChunkName: "documentFieldBuilders." */
    `./${this.typeToComponentName(type)}/builder`);
  }
};
