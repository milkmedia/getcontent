import { Node } from "tiptap";
import { toggleBlockType } from "tiptap-commands";

export default class CssClass extends Node {
  get name() {
    return "cssClass";
  }

  get schema() {
    return {
      attrs: {
        class: {
          default: null
        }
      },
      content: "inline*",
      group: "block",
      defining: true,
      parseDOM: [
        {
          tag: "p[class]",
          priority: 60,
          getAttrs: dom => ({
            class: dom.getAttribute("class")
          })
        }
      ],
      toDOM: node => ["p", { class: node.attrs.class }, 0]
    };
  }

  commands({ type, schema }) {
    return attrs => toggleBlockType(type, schema.nodes.paragraph, attrs);
  }
}
