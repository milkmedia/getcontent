import IntersectionObserver from "intersection-observer";
import Vue from "vue";
import VueRouter from "vue-router";
import SvgIcon from "vue-svgicon";
import Notifications from "vue-notification";
import PortalVue from "portal-vue";
import { VLazyImagePlugin } from "v-lazy-image";
import vClickOutside from "v-click-outside";
import infiniteScroll from "vue-infinite-scroll";
import VCalendar from 'v-calendar';

import MilkUI from "./components/milk-ui";
import Tabs from "@milkmedia/vue-tabs";
import GetContentApp from "./components/GetContentApp";

import routes from "./routes";
import store from "./store";
import "./icons";

Vue.use(VueRouter);
const router = new VueRouter({ routes });

Vue.use(SvgIcon, { tagName: "svg-icon" });
Vue.use(Notifications);
Vue.use(PortalVue);
Vue.use(VLazyImagePlugin);
Vue.use(vClickOutside);
Vue.use(infiniteScroll);
Vue.use(VCalendar);
Vue.use(MilkUI);
Vue.use(Tabs);

new Vue({
  router,
  store,
  el: "#getcontent",
  components: {
    "getcontent-app": GetContentApp
  }
});
