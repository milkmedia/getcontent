import { remove, filter, cloneDeep, isString, get, forEach } from "lodash";
import api from "../../api";

/**
 * Migrates how a template field represents its model key
 * TODO: Deprecate this once sites are updated
 */
function migrateModelKey(field, key) {
  if (!field.model && field.variable) {
    field.model = field.variable;
  }

  if (!field.model && isString(key)) {
    field.model = key;
  }

  return field;
}

function templateResource(template) {
  return {
    name: template.slug,
    editor_class: get(template, "content.editor_class"),
    options: get(template, "content.options", {}),
    replicator:
      get(template, "content.replicator") ||
      get(template, "content.type") === "replicator",
    sets: get(template, "content.sets", []),

    // migrates `variable` or key to be `model`
    // TODO: Deprecate once templates are documented properly
    fields: forEach(get(template.content, "fields", []), migrateModelKey)
  };
}

export default {
  namespaced: true,

  state: {
    all: []
  },

  getters: {
    all: state => {
      return state.all;
    },

    blocks: state => {
      return filter(state.all, template => template.type === "block");
    },

    schema: state => {
      return filter(state.all, template => template.type === "schema");
    },

    byUuid: state => uuid => {
      return templateResource(
        state.all.find(template => template.uuid === uuid)
      );
    },

    bySlug: state => slug => {
      return templateResource(
        state.all.find(template => template.slug === slug)
      );
    }
  },

  mutations: {
    setAll(state, templates) {
      state.all = templates;
    },

    addTemplate(state, template) {
      state.all.push(template);
    },

    updateTemplate(state, { uuid, template }) {
      let templateIndex = state.all.findIndex(
        template => template.uuid === uuid
      );

      state.all[templateIndex] = template;
    },

    removeTemplate(state, uuid) {
      state.all = remove(state.all, template => template.id === uuid);
    }
  },

  actions: {
    loadTemplates({ commit }) {
      return new Promise((resolve, reject) => {
        api.get("templates").then(payload => {
          resolve();
          return commit("setAll", payload.data.data);
        }, reject);
      });
    },

    saveTemplate({ commit }, template) {
      let payload = cloneDeep(template);

      return new Promise((resolve, reject) => {
        if (template.id) {
          return api.put(`templates/${template.id}`, payload).then(payload => {
            commit("updateTemplate", {
              uuid: template.uuid,
              template: payload.data.data
            });
            resolve(template.uuid);
          }, reject);
        }

        return api.post("templates", payload).then(payload => {
          commit("addTemplate", payload.data.data);
          resolve(payload.data.data.uuid);
        }, reject);
      });
    },

    deleteTemplate({ commit }, template) {
      return new Promise((resolve, reject) => {
        return api
          .delete(`templates/${template.id}`, template)
          .then(payload => {
            resolve();
            commit("removeTemplate", { uuid: template.uuid });
          }, reject);
      });
    }
  }
};
