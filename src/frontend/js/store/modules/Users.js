import User from "../../models/User";

export default {
  namespaced: true,

  state: {
    me: {},
    all: []
  },

  getters: {
    me: state => {
      return state.me;
    },

    all: state => {
      return state.all;
    },
  },

  mutations: {
    setMe(state, user) {
      state.me = user;
    },

    setAll(state, users) {
      state.all = users;
    },
  },

  actions: {
    loadMe({ commit }) {
      return User.find('me').then(payload => {
        commit("setMe", payload.data);
      });
    },
  }
};
