import { filter } from "lodash";
import api from "../../api";

export default {
  namespaced: true,

  state: {
    directories: [],
    files: []
  },

  getters: {
    directories: state => {
      return state.directories;
    },

    directory: state => path => {
      return state.directories.find(directory => directory.path === path);
    },

    files: state => {
      return state.files;
    },

    all: state => {
      return {
        files: state.files,
        directories: state.directories
      };
    }
  },

  mutations: {
    setFiles(state, files) {
      state.files = files;
    },

    setDirectories(state, directories) {
      state.directories = directories;
    },

    addDirectory(state, directory) {
      state.directories.push(directory);
    },

    addFile(state, file) {
      state.files.push(file);
    },

    removeFile(state, file) {
      state.files = filter(state.files, thisFile => thisFile !== file);
    },

    removeDirectory(state, path) {
      state.directories = filter(
        state.directories,
        directory => directory !== path
      );
    }
  },

  actions: {
    loadFromPath({ commit }, path) {
      return new Promise((resolve, reject) => {
        api.get(`files/${path || ""}`).then(payload => {
          commit("setFiles", payload.data.data.files);
          commit("setDirectories", payload.data.data.directories);
          return resolve();
        }, reject);
      });
    },

    deleteFile({ commit }, file) {
      return new Promise((resolve, reject) => {
        return api.delete(`files/${file.path}`).then(payload => {
          resolve();
          commit("removeFile", file);
        }, reject);
      });
    },

    createDirectory({ commit }, { name, path }) {
      return new Promise((resolve, reject) => {
        return api
          .post(`files/${path || ""}`, {
            directory: name
          })
          .then(payload => {
            resolve();
            commit("addDirectory", payload.data.data);
          }, reject);
      });
    },

    deleteDirectory({ commit }, directory) {
      return new Promise((resolve, reject) => {
        return api.delete(`files/${directory.path}`).then(payload => {
          resolve();
          commit("removeDirectory", directory);
        }, reject);
      });
    }
  }
};
