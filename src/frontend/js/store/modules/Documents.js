import uuidv4 from "uuid/v4";
import { remove, filter } from "lodash";
import api from "../../api";
import Document from "../../models/Document";

export default {
  namespaced: true,

  state: {
    all: []
  },

  getters: {
    all: state => {
      return state.all;
    },

    byUuid: state => uuid => {
      return state.all.find(document => document.uuid === uuid);
    },

    inRoot: state => {
      return filter(state.all, document => document.group_id === null);
    },

    inGroup: state => id => {
      return filter(state.all, document => document.group_id === id);
    }
  },

  mutations: {
    setAll(state, documents) {
      state.all = documents;
    },

    addDocument(state, document) {
      state.all.push(document);
    },

    updateDocument(state, { uuid, document }) {
      let documentIndex = state.all.findIndex(
        document => document.uuid === uuid
      );

      state.all[documentIndex] = document;
    },

    removeDocument(state, uuid) {
      state.all = remove(state.all, document => document.uuid === uuid);
    }
  },

  actions: {
    /**
     * Load Documents from the API
     * @param commit
     * @param group_id
     * @returns {Promise<any>}
     */
    loadDocuments({ commit }, group_id = null) {
      return new Promise((resolve, reject) => {
        Document.where("group", group_id)
          .get()
          .then(payload => {
            resolve();
            return commit("setAll", payload.data);
          }, reject);
      });
    },

    saveDocument({ commit }, document) {
      document.content = document.schema;

      return new Promise((resolve, reject) => {
        if (document.id) {
          return api.put(`documents/${document.id}`, document).then(payload => {
            resolve(payload.data.data);
            commit("updateDocument", {
              uuid: document.uuid,
              document: payload.data.data
            });
          }, reject);
        }

        document.uuid = uuidv4();
        return api.post("documents", document).then(payload => {
          resolve(payload.data.data);
          commit("addDocument", payload.data.data);
        }, reject);
      });
    },

    deleteDocument({ commit }, document) {
      return new Promise((resolve, reject) => {
        return api
          .delete(`documents/${document.id}`, document)
          .then(payload => {
            resolve();
            commit("removeDocument", { uuid: document.uuid });
          }, reject);
      });
    }
  }
};
