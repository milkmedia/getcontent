import Vue from "vue";
import Vuex from "vuex";

import Users from "./modules/Users";
import Documents from "./modules/Documents";
import Groups from "./modules/Groups";
import Files from "./modules/Files";
import Templates from "./modules/Templates";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Users,
    Documents,
    Groups,
    Files,
    Templates
  }
});
