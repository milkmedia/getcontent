import axios from "axios";

let api = axios.create();
api.defaults.baseURL = '/api/';

let api_token = window.config.api_token;
let csrf_token = window.config.csrf_token;

if (api_token) {
  api.defaults.headers.common["Authorization"] = `Bearer ${api_token}`;
}

if (csrf_token) {
  api.defaults.headers.common["X-CSRF-TOKEN"] = `${csrf_token}`;
}

export default api;
