import Model from "./Model";
import Document from "./Document";
import { find, get } from "lodash";

export default class Group extends Model {
  resource() {
    return "groups";
  }

  getMetaOption(option, defaultValue = null) {
    return get(this.meta, option, defaultValue)
  }

  documents () {
    return this.hasMany(Document)
  }
}
