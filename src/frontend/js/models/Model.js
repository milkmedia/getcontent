import api from "../api";
import axios from "axios";
import { Model as BaseModel } from "vue-api-query";

BaseModel.$http = api;

export default class Model extends BaseModel {
  abort() {
    this.cancelSource.cancel();
  }

  // define a base url for a REST API
  baseURL() {
    return "";
  }

  // implement a default request method
  request(config) {
    const CancelToken = axios.CancelToken;
    this.cancelSource = CancelToken.source();
    config.cancelToken = this.cancelSource.token;

    return this.$http.request(config);
  }
}
