import Model from "./Model";
import { find, each, assign, get } from "lodash";
import moment from "moment";

export default class Document extends Model {
  resource() {
    return "documents";
  }

  linkedDocument(id) {
    return this.documents
      ? find(this.documents, document => document.id === id)
      : null;
  }

  attachDocument(document) {
    if (!this.linkedDocument(document.id)) {
      this.documents.push(document);
    }
  }

  removeDocument(document) {
    this.documents.splice(this.documents.indexOf(document), 1);
  }

  get published() {
    return this.publishedMoment ? this.publishedMoment.calendar() : null;
  }

  get publishedMoment() {
    return this.published_at ? moment(this.published_at) : null;
  }

  get isScheduled() {
    return this.publishedMoment && this.publishedMoment.isAfter();
  }

  get created() {
    return this.createdMoment.calendar();
  }

  get createdMoment() {
    return moment(this.created_at);
  }

  get updated() {
    return this.updatedMoment.calendar();
  }

  get updatedMoment() {
    return moment(this.updated_at);
  }

  getMetaOption(option, defaultValue = null) {
    return get(this.meta, option, defaultValue)
  }

  /**
   * Upgrades `content` to model and schema format
   * TODO: Deprecate this once sites are updated
   */
  upgradeModelSchema() {
    this.document.model = {};
    this.document.schema = [];

    each(this.content.fields, field => {
      let variable = field.variable || this.nextOfType(field.type);

      // create a model of the data originally in `content.fields.*.model`
      this.model[variable] = assign(
        field.model ? { value: field.model } : {},
        { options: assign({ css_class: field.css_class }, field.options) }
      );

      // create a schema of the fields used
      let component = this.migrateBlockComponents(
        {
          type: field.type,
          model: variable
        },
        field,
        variable
      );

      this.schema.push(component);
    });
  }

  /**
   * TODO: Deprecate this once sites are updated
   */
  migrateBlockComponents(componentSchema, component, scopeKey) {
    if (component.type === "block") {
      componentSchema.type = "template";

      if (component.block) {
        componentSchema.template = component.block.slug;
      }

      this.model[scopeKey].value = [];

      if (component.fields) {
        each(component.fields, (field, fieldKey) => {
          this.model[scopeKey][field.variable || fieldKey] = field.model || null;
          let setFields = {};
          if (field.display) {
            setFields.type = field.display.toLowerCase();
          }

          each(field.fields, (setField, setFieldKey) => {
            setFields[setFieldKey] = { value: setField.model };
          });

          this.model[scopeKey].value.push(setFields);
        });
      }
    }

    return componentSchema;
  }

  blank() {
    return new Document({
      name: null,
      group_id: null,
      model: {},
      schema: [],
      status: "draft",
      template: null
    });
  }
}
