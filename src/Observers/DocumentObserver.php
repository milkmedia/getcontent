<?php

namespace MilkMedia\GetContent\Observers;

use Illuminate\Support\Str;
use MilkMedia\GetContent\Document;

class DocumentObserver
{
    /**
     * Handle to the document "creating" event.
     *
     * @param Document $document
     *
     * @return void
     */
    public function creating(Document $document)
    {
        if (!$document->slug) {
            $document->slug = Str::slug($document->name);
        }

        if (!$document->uuid) {
            $document->uuid = (string) Str::uuid();
        }
    }
}
