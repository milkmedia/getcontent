<?php

namespace MilkMedia\GetContent\Observers;

use Illuminate\Support\Str;
use MilkMedia\GetContent\Template;

class TemplateObserver
{
    /**
     * Handle to the group "creating" event.
     *
     * @param Template $template
     *
     * @return void
     */
    public function creating(Template $template)
    {
        $template->slug = Str::slug($template->name);

        if (!$template->uuid) {
            $template->uuid = (string) Str::uuid();
        }
    }

    /**
     * Handle to the group "updating" event.
     *
     * @param Template $template
     */
    public function updating(Template $template)
    {
        $template->slug = Str::slug($template->name);
    }
}
