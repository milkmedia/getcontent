<?php

namespace MilkMedia\GetContent\Observers;

use Illuminate\Support\Str;
use MilkMedia\GetContent\Group;

class GroupObserver
{
    /**
     * Handle to the group "creating" event.
     *
     * @param Group $group
     *
     * @return void
     */
    public function creating(Group $group)
    {
        if (!$group->slug) {
            $group->slug = Str::slug($group->name);
        }

        if (!$group->uuid) {
            $group->uuid = (string) Str::uuid();
        }
    }
}
