<?php

namespace MilkMedia\GetContent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use MilkMedia\GetContent\Traits\HasSchemalessContent;

/**
 * @property mixed content
 */
class Template extends Model
{
    use HasSchemalessContent;

    protected $guarded = [];

    public $casts = [
        'content' => 'array',
    ];

    /**
     * Merges schema with passed model values.
     *
     * @param $templateModel
     *
     * @return array
     */
    public function fields($templateModel): array
    {
        $content = [];

        collect($this->content->fields)->each(function ($field) use (
            $templateModel,
            &$content
        ) {
            //@todo depreciate `variable`
            $modelKey = data_get($field, 'model', data_get($field, 'variable'));
            $model = data_get($templateModel, $modelKey);

            $content[$modelKey] = [
                'type' => data_get($field, 'type'),
                'model' => \is_array($model) ? Arr::except($model, 'options') : $model,
                'options' => data_get($model, 'options'),
            ];
        });

        return $content;
    }
}
