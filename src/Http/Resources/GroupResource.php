<?php

namespace MilkMedia\GetContent\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uuid' => $this->uuid,
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'name' => $this->name,
            'slug' => $this->slug,
            'schema' => $this->schema,
            'meta' => $this->meta,
            'parent' => $this->whenLoaded('parent'),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            $this->mergeWhen($request->exists('withChildren'), function () {
                return [
                    'groups' => GroupResource::collection($this->children),
                    'documents' => DocumentResource::collection($this->documents),
                ];
            }),
        ];
    }
}
