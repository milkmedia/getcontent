<?php

namespace MilkMedia\GetContent\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'name' => $this->name,
            'slug' => $this->slug,
            'group_id' => $this->group_id,
            'group' => $this->whenLoaded('group'),
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
