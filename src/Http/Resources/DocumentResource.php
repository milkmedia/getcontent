<?php

namespace MilkMedia\GetContent\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'name' => $this->name,
            'description' => $this->description,
            'slug' => $this->slug,
            'status' => $this->status,
            'model' => $this->model ? $this->model->all() : [],
            'schema' => $this->content ? $this->content->all() : [],
            'meta' => $this->meta,
            'group_id' => $this->group_id,
            'group' => $this->whenLoaded('group'),
            'documents' => DocumentIndexResource::collection($this->whenLoaded('documents')),
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
