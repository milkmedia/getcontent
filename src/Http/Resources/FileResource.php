<?php

namespace MilkMedia\GetContent\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use MilkMedia\GetContent\GetContent;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'       => basename($this->resource),
            'path'       => Str::replaceFirst(GetContent::filePath(), '', $this->resource),
            'url'        => Storage::url($this->resource),
            'size'       => Storage::getSize($this->resource),
            'mimeType'  => Storage::getMimetype($this->resource),
            'createdAt' => (new Carbon())->timestamp(Storage::getTimestamp($this->resource))->toDateTimeString(),
        ];
    }
}
