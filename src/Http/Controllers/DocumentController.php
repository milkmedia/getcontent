<?php

namespace MilkMedia\GetContent\Http\Controllers;

use Illuminate\Http\Request;
use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\GetContent;

class DocumentController extends Controller
{
    public function show(Request $request, $slug = null)
    {
        $documentQuery = Document::bySlug($slug);

        if ($request->has('preview') && auth()) {
            $documentQuery->preview();
        }

        $document = $documentQuery->first();
        $views = GetContent::viewsFromSegments($request->segments(), $document && $document->exists);

        if (!$slug) {
            $views->prepend('home');
        }

        try {
            return view()->first($views->toArray(), [
                'document' => $document,
                'request'  => $request,
            ]);
        } catch (\Exception $exception) {
            return abort(404);
        }
    }
}
