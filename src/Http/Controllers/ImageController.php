<?php

namespace MilkMedia\GetContent\Http\Controllers;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\File;
use League\Glide\Responses\LaravelResponseFactory;
use League\Glide\ServerFactory;

class ImageController extends Controller
{
    /**
     * Glide image manipulation
     *
     * @param Filesystem $filesystem
     * @param            $path
     *
     * @return mixed
     */
    public function show(Filesystem $filesystem, $path)
    {
        if(File::mimeType($path) === 'image/svg') {
            return \response()->redirectTo($path);
        }

        $path = preg_replace('/^storage/', 'public', $path);

        $server = ServerFactory::create([
            'response' => new LaravelResponseFactory(app('request')),
            'source' => $filesystem->getDriver(),
            'cache' => $filesystem->getDriver(),
            'cache_path_prefix' => '.cache',
            'base_url' => 'img',
        ]);

        return $server->getImageResponse($path, request()->all());
    }
}
