<?php

namespace MilkMedia\GetContent\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Group;
use MilkMedia\GetContent\Http\Controllers\Controller;
use MilkMedia\GetContent\Http\Resources\DocumentIndexResource;
use MilkMedia\GetContent\Http\Resources\DocumentResource;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('index', Document::class);

        if ($group = Group::find($request->input('filter.group'))) {
            $this->authorize('view', $group);
        }

        $allDocuments = Document::withoutGlobalScope('published')
            ->withoutGlobalScope('approved');

        $documents = QueryBuilder::for($allDocuments)
            ->allowedFilters(
                'uuid',
                'name',
                'slug',
                AllowedFilter::exact('group', 'group_id')
            )
            ->allowedIncludes('documents', 'group')
            ->defaultSort('-created_at')
            ->allowedSorts('created_at', 'updated_at', 'name', 'slug');

        if ($request->input('filter.group') === null) {
            // Limit to Users root if set
            if ($rootGroupId = Auth::user()->getSetting('permissions.groupRootId')) {
                $documents->where('group_id', $rootGroupId);
            } else {
                $documents->whereNull('group_id');
            }
        }

        return DocumentIndexResource::collection($documents->paginate($request->input('limit', 50)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Document::class);

        $valid = $request->validate([
            'name' => 'required|string',
            'slug' => 'string|nullable',
            'status' => 'string',
            'uuid' => 'string|nullable|unique:documents,uuid',
            'content' => 'array',
            'schema' => 'array',
            'model' => 'array',
            'meta' => 'array',
            'published_at' => 'nullable|string',
            'group_id' => 'nullable|exists:groups,id',
            'documents' => 'nullable|array',
        ]);

        if ($group = Group::find($request->input('group_id'))) {
            $this->authorize('view', $group);
        }

        $document = Document::create(
            array_merge(
                Arr::except($valid, 'documents'),
                ['owner_id' => Auth::id()]
            )
        );

        if ($links = data_get($valid, 'documents')) {
            $document->documents()->sync(Arr::pluck($links, 'id'));
        }

        return response()->json(['data' => $document], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Document $document
     *
     * @return DocumentResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Request $request, Document $document)
    {
        $this->authorize('view', $document);

        $document->loadMissing('group');

        if (Str::contains($request->input('include'), 'documents')) {
            $document->loadMissing('documents');
        }

        return new DocumentResource($document);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Document $document
     *
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Document $document)
    {
        $this->authorize('update', $document);

        $valid = $request->validate([
            'name' => 'string',
            'slug' => 'string',
            'status' => 'string',
            'content' => 'array',
            'model' => 'array',
            'meta' => 'nullable|array',
            'published_at' => 'nullable|date',
            'group_id' => 'nullable|exists:groups,id',
            'documents' => 'nullable|array',
        ]);

        $document->update(Arr::except($valid, 'documents'));

        if (Arr::has($valid, 'documents')) {
            $linked = data_get($valid, 'documents');
            $document->documents()->sync(is_array($linked) ? Arr::pluck($linked, 'id') : $linked);
        }

        return ['data' => $document];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Document $document
     *
     * @return void
     * @throws Exception
     */
    public function destroy(Request $request, Document $document)
    {
        $this->authorize('delete', $document);

        if ($request->has('force')) {
            $document->forceDelete();

            return;
        }

        $document->delete();
    }
}
