<?php

namespace MilkMedia\GetContent\Http\Controllers\Api;

use Exception;
use Fuse\Fuse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Group;
use MilkMedia\GetContent\Http\Controllers\Controller;
use MilkMedia\GetContent\Http\Resources\DocumentIndexResource;
use MilkMedia\GetContent\Http\Resources\DocumentResource;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;

class DocumentSearchController extends Controller
{
    /**
     * Search Documents and return a listing of the results.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request)
    {
        $searchQuery = $request->get('q');
        if (!$searchQuery) {
            abort(400, 'No search query sent');
        }

        $this->authorize('index', Document::class);

        if ($request->input('filter.group')
            && $group = Group::find($request->input('filter.group'))) {
            $this->authorize('view', $group);
        }

        // get all documents for Fuse search
        // @todo this should be using laravel/scout or some other proper search index
        $allDocuments = Document::withoutGlobalScope('published')
            ->withoutGlobalScope('approved')
            ->select(['id', 'name', 'slug']);

        if (isset($group)) {
            $allDocuments->where('group_id', $group->id);
        }

        if (!isset($group) && $userRootGroupId = auth()->user()->getSetting('permissions.groupRootId')) {
            $permittedGroups = Group::select('id')->descendantsAndSelf($userRootGroupId)
                ->toFlatTree();

            $allDocuments->whereIn('group_id', $permittedGroups);
        }

        $fuse = new Fuse($allDocuments->get()->toArray(), [
            'keys' => ['name', 'slug'],
            'threshold' => 0.4,
        ]);

        $fuseResults = collect($fuse->search($searchQuery));

        $documentsQuery = Document::withoutGlobalScope('published')
            ->with('group')
            ->withoutGlobalScope('approved')
            ->whereIn('id', $fuseResults->pluck('id'));

        return DocumentIndexResource::collection($documentsQuery->paginate($request->input('limit', 50)));
    }
}
