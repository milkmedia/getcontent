<?php

namespace MilkMedia\GetContent\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use MilkMedia\GetContent\Group;
use MilkMedia\GetContent\Http\Controllers\Controller;
use MilkMedia\GetContent\Http\Resources\GroupResource;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request)
    {
        if ($group = Group::find($request->input('filter.parent'))) {
            $this->authorize('view', $group);
        }

        $groups = QueryBuilder::for(Group::class)
            ->allowedFilters(
                'uuid',
                'name',
                'slug',
                AllowedFilter::exact('parent', 'parent_id')
            )
            ->allowedIncludes('parent', 'children')
            ->defaultSort('name')
            ->allowedSorts('created_at', 'updated_at', 'name', 'slug');

        if (!$request->has('filter.parent')
            || $request->input('filter.parent') === null) {
            // Limit to Users root if set
            if ($rootGroupId = Auth::user()->getSetting('permissions.groupRootId')) {
                $groups->where('parent_id', $rootGroupId);
            } else {
                $groups->whereNull('parent_id');
            }
        }

        return GroupResource::collection($groups->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return array
     */
    public function store(Request $request)
    {
        $valid = $request->validate([
            'name' => 'required|string',
            'slug' => 'string',
            'parent_id' => 'numeric|exists:groups,id|nullable',
            'schema' => 'array|nullable',
            'meta' => 'array|nullable',
        ]);

        $group = Group::create($valid);

        return response()->json(['data' => $group], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Group $group
     *
     * @return GroupResource
     */
    public function show(Group $group)
    {
        $group->loadMissing('parent');
        return new GroupResource($group);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Group $group
     *
     * @return array
     */
    public function update(Request $request, Group $group)
    {
        $valid = $request->validate([
            'name' => 'required|string',
            'slug' => 'string',
            'parent_id' => 'numeric|exists:groups|nullable',
            'schema' => 'array|nullable',
            'meta' => 'array|nullable',
        ]);

        $group->update($valid);

        return ['data' => $group];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Group $group
     *
     * @return void
     * @throws Exception
     */
    public function destroy(Group $group)
    {
        $group->delete();
    }
}
