<?php

namespace MilkMedia\GetContent\Http\Controllers\Api;

use Illuminate\Http\Request;
use MilkMedia\GetContent\Http\Controllers\Controller;
use MilkMedia\GetContent\Http\Resources\TemplateResource;
use MilkMedia\GetContent\Template;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $templates = Template::all();

        return TemplateResource::collection($templates);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function store(Request $request)
    {
        $valid = $request->validate([
            'name'    => 'required|string',
            'type'    => 'required|string|in:schema,block',
            'content' => 'array',
        ]);

        $template = Template::create($valid);

        return response()->json(new TemplateResource($template), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Template $template
     *
     * @return TemplateResource
     */
    public function show(Template $template)
    {
        return new TemplateResource($template);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Template                  $template
     *
     * @return TemplateResource
     */
    public function update(Request $request, Template $template)
    {
        $valid = $request->validate([
            'name'    => 'string',
            'type'    => 'string|in:schema,block',
            'content' => 'array',
        ]);

        $template->update($valid);

        return new TemplateResource($template);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Template $template
     *
     * @return void
     * @throws \Exception
     */
    public function destroy(Template $template)
    {
        $template->delete();
    }
}
