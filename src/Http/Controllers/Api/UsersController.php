<?php

namespace MilkMedia\GetContent\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Http\Controllers\Controller;
use MilkMedia\GetContent\Http\Resources\DocumentResource;
use MilkMedia\GetContent\Http\Resources\UserResource;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;

class UsersController extends Controller
{
    public function authorised(Request $request)
    {
        return new UserResource($request->user());
    }
}
