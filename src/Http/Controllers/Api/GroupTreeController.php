<?php

namespace MilkMedia\GetContent\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use MilkMedia\GetContent\Group;
use MilkMedia\GetContent\Http\Controllers\Controller;
use MilkMedia\GetContent\Http\Resources\GroupResource;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class GroupTreeController extends Controller
{
    /**
     * Return a flat tree of all Groups
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $groups = QueryBuilder::for(Group::class)
            ->defaultSort('name')
            ->allowedSorts('created_at', 'updated_at', 'name', 'slug');

        if ($rootGroupId = Auth::user()->getSetting('permissions.groupRootId')) {
            $groups->where('parent_id', $rootGroupId);
        }

        $nodes = $groups->get()->toFlatTree();

        return GroupResource::collection($nodes);
    }
}
