<?php

namespace MilkMedia\GetContent\Http\Controllers\Api;

use Fuse\Fuse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use League\Flysystem\FileNotFoundException;
use MilkMedia\GetContent\GetContent;
use MilkMedia\GetContent\Http\Controllers\Controller;
use MilkMedia\GetContent\Http\Resources\DirectoryResource;
use MilkMedia\GetContent\Http\Resources\FileResource;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param null $directory
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request, $directory = null)
    {
        $path = GetContent::filePath($directory);

        if ($q = $request->get('q')) {
            $files = FileResource::collection(collect(Storage::allFiles($path)))->toArray($request);
            $fuse = new Fuse($files, [
                'keys' => ['name'],
            ]);

            return response()->json([
                'data' => [
                    'files' => $fuse->search($q),
                    'directories' => [],
                ],
            ]);
        }

        $files = Storage::files($path);
        $directories = Storage::directories($path);

        return response()->json([
            'data' => [
                'files' => FileResource::collection(collect($files)),
                'directories' => DirectoryResource::collection(collect($directories)),
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @param null $directory
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $directory = null)
    {
        $path = GetContent::filePath($directory);

        $valid = $request->validate([
            'file' => 'required_without:directory|file',
            'directory' => 'required_without:file|string',
        ]);

        if (Arr::has($valid, 'directory')) {
            // Make new directory
            // Sanitise path removing attempts to reach out the storage path
            $newDirectory = trim($valid['directory'], '/');
            Storage::makeDirectory("{$path}/{$newDirectory}");

            return response()->json(['data' => new DirectoryResource("{$path}/{$newDirectory}")], 201);
        }

        if (Arr::has($valid, 'file')) {
            // Upload file
            $pathinfo = pathinfo($valid['file']->getClientOriginalName());
            $path = Storage::putFileAs(
                $path,
                $valid['file'],
                Str::slug($pathinfo['filename']) . ".{$pathinfo['extension']}"
            );

            return response()->json(['data' => new FileResource($path)], 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $path
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($path)
    {
        $path = GetContent::filePath($path);

        try {

            if (Storage::getMimetype($path) === 'directory') {
                Storage::deleteDirectory($path);
            } else {
                Storage::delete($path);
            }
        } catch (FileNotFoundException $e) {
            abort(404);
        }

        return response()->json(null, 202);
    }
}
