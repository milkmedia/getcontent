<?php

namespace MilkMedia\GetContent;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use MilkMedia\GetContent\Traits\HasSchemalessContent;
use MilkMedia\GetContent\Traits\HasSchemalessModel;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Document extends Model implements AuditableContract
{
    use SoftDeletes;
    use HasSchemalessContent;
    use HasSchemalessModel;
    use Auditable;

    protected $guarded = [];

    public $casts = [
        'content' => 'array',
        'model' => 'array',
        'meta' => 'array',
    ];

    protected $dates = [
        'published_at',
        'deleted_at',
    ];

    protected $appends = ['schema'];

    const STATUS_DRAFT = 'draft';
    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('published', function (Builder $builder) {
            $builder->where('published_at', '<', now())
                ->orWhereNull('published_at');
        });

        static::addGlobalScope('approved', function (Builder $builder) {
            $builder->where('status', self::STATUS_APPROVED);
        });
    }

    public function owner()
    {
        return $this->belongsTo(config('getcontent.models.owner'), 'owner_id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function documents()
    {
        return $this->belongsToMany(self::class, 'document_links', 'document_id', 'linked_document_id');
    }

    public function referencedBy()
    {
        return $this->belongsToMany(self::class, 'document_links', 'linked_document_id', 'document_id');
    }

    public function linked($key)
    {
        if (!Arr::has($this->model, "{$key}.value")) {
            return false;
        }

        return $this->documents->whereIn('id', data_get($this->model, "{$key}.value"))->first();
    }

    /**
     * @param $query
     * @param $slug
     *
     * @return mixed
     */
    public function scopeBySlug($query, $slug)
    {
        $query->where('slug', $slug);

        collect(explode('/', $slug))->each(function ($segment) use ($query) {
            $query->orWhere('slug', $segment);
        });

        $query->orderByRaw('position(? in "slug") desc', $slug);

        return $query;
    }

    /**
     * @param $query
     * @param $groupId
     *
     * @return
     */
    public function scopeWithinGroup($query, $groupId)
    {
        $descendantGroups = Group::descendantsAndSelf($groupId)->pluck('id');

        $query->whereIn('group_id', $descendantGroups);

        return $query;
    }

    /**
     * Removes global scopes that restrict to
     * published and approved documents
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopePreview($query)
    {
        return $query
            ->withoutGlobalScope('published')
            ->withoutGlobalScope('approved');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getFieldsAttribute()
    {
        if ($this->model && count($this->model)) {
            return $this->getFieldsFromModel();
        }

        // pluck variables from content fields to key the content model
        return collect($this->content->fields)->keyBy(function ($field, $key) {
            return data_get($field, 'variable', $key);
        })->transform(function ($field) {
            if (data_get($field, 'type') !== 'block') {
                return $field;
            }

            $field['fields'] = collect($field['fields'])->keyBy(function ($blockField, $key) {
                return data_get($blockField, 'variable', $key);
            });

            return $field;
        });
    }

    /**
     * Proxy to legacy content field
     * @return mixed
     * @todo migrate documents.content to documents.schema
     */
    public function getSchemaAttribute()
    {
        return data_get($this->content, 'fields', $this->content);
    }

    /**
     * Proxy to legacy content field
     *
     * @param $value
     */
    public function setSchemaAttribute($value)
    {
        $this->content = $value;
    }

    /**
     * Generates content fields from model and schema
     */
    public function getFieldsFromModel()
    {
        $content = [];

        collect($this->schema)->each(function ($field) use (&$content) {
            $model = data_get($this->model, data_get($field, 'model'));

            $content[data_get($field, 'model')] = [
                'type' => data_get($field, 'type'),
                'options' => data_get($model, 'options'),
                'model' => Arr::except($model, 'options'),
            ];

            if (data_get($field, 'type') === 'template') {
                $template = Template::whereSlug(data_get($field, 'template'))->first();
                $content[data_get($field, 'model')]['fields'] = $template->fields($model);
                $content[data_get($field, 'model')]['template'] = $template->slug;
            }
        });

        return $content;
    }

    /**
     * Laravel seems to be very particular about date formatting when casting,
     * so just parsing whatever is set through Carbons best effort
     *
     * @param $value
     */
    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ? Carbon::parse($value) : null;
    }

    /**
     * Gets the specified key from the Document model
     * @deprecated
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return $this->getValue($key, $default);
    }

    /**
     * Gets the specified key from the Document model,
     * looking for the `value` sub key first
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function getValue($key, $default = null)
    {
        return data_get($this->model, "{$key}.value", data_get($this->model, $key, $default));
    }
}
