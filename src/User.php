<?php

namespace MilkMedia\GetContent;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Arr;

/**
 * @property array settings
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'settings' => 'array'
    ];

    /**
     * Return the stored Group root ID
     *
     * @return mixed
     */
    public function getSetting($path)
    {
        return Arr::get($this->settings, $path);
    }

    /**
     * Return the Users permitted root Group
     * @return Group|null
     */
    public function getRootGroupAttribute()
    {
        if ($groupId = $this->getSetting('permissions.groupRootId')) {
            return Group::find($groupId)->first();
        }

        return null;
    }
}
