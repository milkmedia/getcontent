<?php

namespace MilkMedia\GetContent\Test;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    use TestCaseTrait;
}
