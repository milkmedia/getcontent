<?php

namespace MilkMedia\GetContent\Test\Features\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\GetContent;
use MilkMedia\GetContent\Group;
use MilkMedia\GetContent\Test\TestCase;

class GetContentQueriesTest extends TestCase
{
    /** @test */
    public function scope_lookup_to_group_and_descendants()
    {
        $parent1 = factory(Group::class)->create(['slug' => 'public']);
        $parent2 = factory(Group::class)->create(['slug' => 'internal']);
        $child = factory(Group::class)->create(['parent_id' => $parent1->id]);
        $descendant = factory(Group::class)->create(['parent_id' => $child->id]);

        $document1 = factory(Document::class)->create(['slug' => 'about-us', 'group_id' => $descendant->id]);
        $document2 = factory(Document::class)->create(['slug' => 'about-us', 'group_id' => $parent2->id]);

        $found1 = Document::withinGroup(1)->bySlug('about-us')->first();
        $found2 = Document::withinGroup(2)->bySlug('about-us')->first();

        $this->assertEquals($document1->id, $found1->id);
        $this->assertEquals($document2->id, $found2->id);
    }

    /** @test */
    public function get_closest_slug_match()
    {
        factory(Document::class)->create(['slug' => 'about-us']);
        factory(Document::class)->create(['slug' => 'about-us/team']);
        factory(Document::class)->create(['slug' => 'about-us/team/bob']);

        $document = Document::bySlug('about-us/team/bob')->first();
        $this->assertEquals(3, $document->id);

        $document = Document::bySlug('about-us/team')->first();
        $this->assertEquals(2, $document->id);

        $document = Document::bySlug('about-us')->first();
        $this->assertEquals(1, $document->id);
    }
}
