<?php

namespace MilkMedia\GetContent\Test\Features\Unit;

use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Test\TestCase;

class DocumentContentFieldsTest extends TestCase
{
    /** @test */
    public function getting_fields_keys_by_variable()
    {
        $document = new Document;
        $document->content = [
            'fields' => [
                [
                    'variable' => 'my-field',
                    'model'    => 'Field Value',
                ],
                [
                    'type'  => 'text',
                    'model' => 'Another field',
                ],
                [
                    'type'  => 'text',
                    'model' => 'Third field',
                ],
            ],
        ];

        $this->assertArrayHasKey('my-field', $document->fields);
    }

    /** @test */
    public function block_fields_recursly_key_by_variable()
    {
        $document = new Document;
        $document->content = [
            'fields' => [
                [
                    'type'  => 'content',
                    'model' => 'Field Value',
                ],
                [
                    'type'     => 'block',
                    'variable' => 'block-field',
                    'fields'   => [
                        [
                            'variable' => 'first-block-field',
                            'type'     => 'text',
                            'model'    => 'Block field value',
                        ],
                        [
                            'type' => 'media',
                        ],
                    ],
                ],
            ],
        ];

        $this->assertArrayHasKey('first-block-field', $document->fields['block-field']['fields']);
        $this->assertEquals('text', $document->fields['block-field']['fields']['first-block-field']['type']);
    }

    /** @test */
    public function returns_named_fields_from_model_and_schema()
    {
        $document = new Document;
        $document->model = [
            'content1' => ['value' => '<p>Some html content</p>'],
            'media1'   => ['value' => [['url' => 'https://example.com/image.jpg']], 'options' => ['image_size' => 'sm']],
        ];
        $document->content = [
            ['type' => 'content', 'model' => 'content1'],
            ['type' => 'media', 'model' => 'media1'],
        ];

        $FieldsFromModel = $document->getFieldsFromModel();
        $this->assertEquals([
            'content1' => ['type' => 'content', 'model' => ['value' => '<p>Some html content</p>'], 'options' => null],
            'media1'   => ['type'    => 'media',
                           'model'   => ['value' => [['url' => 'https://example.com/image.jpg']]],
                           'options' => ['image_size' => 'sm'],
            ],
        ], $FieldsFromModel);
    }
}
