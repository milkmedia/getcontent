<?php

namespace MilkMedia\GetContent\Test\Features\Unit;

use MilkMedia\GetContent\Template;
use MilkMedia\GetContent\Test\TestCase;

class TemplateTest extends TestCase
{
    /** @test */
    public function it_returns_fields_with_models()
    {
        $template = factory(Template::class)->make([
            'content' => [
                'fields' => [
                    [
                        'type'  => 'text',
                        'model' => 'header',
                    ],
                    [
                        'type'  => 'text',
                        'model' => 'subheader',
                    ],
                ],
            ],
        ]);

        $model = [
            'header'    => 'This Page',
            'subheader' => 'About something special',
        ];

        $this->assertEquals([
            'header'    => [
                'type'    => 'text',
                'model'   => 'This Page',
                'options' => null,
            ],
            'subheader' => [
                'type'    => 'text',
                'model'   => 'About something special',
                'options' => null,
            ],
        ], $template->fields($model));
    }
}
