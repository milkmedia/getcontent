<?php

namespace MilkMedia\GetContent\Test;

use Glorand\Model\Settings\ModelSettingsServiceProvider;
use Illuminate\Support\Facades\Route;
use Kalnoy\Nestedset\NestedSetServiceProvider;
use MilkMedia\GetContent\GetContent;
use MilkMedia\GetContent\GetContentServiceProvider;
use MilkMedia\GetContent\User;
use Spatie\QueryBuilder\QueryBuilderServiceProvider;
use Spatie\SchemalessAttributes\SchemalessAttributesServiceProvider;

trait TestCaseTrait
{
    protected $user;

    /**
     * Make sure all integration tests use the same Laravel "skeleton" files.
     * This avoids duplicate classes during migrations.
     *
     * Overrides \Orchestra\Testbench\Dusk\TestCase::getBasePath
     *       and \Orchestra\Testbench\Concerns\CreatesApplication::getBasePath
     *
     * @return string
     */
    protected function getBasePath()
    {
        // Adjust this path depending on where your override is located.
        return __DIR__.'/../vendor/orchestra/testbench-dusk/laravel';
    }

    protected function getPackageProviders($app)
    {
        return [
            GetContentServiceProvider::class,
            SchemalessAttributesServiceProvider::class,
            NestedSetServiceProvider::class,
            QueryBuilderServiceProvider::class,
        ];
    }

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->loadLaravelMigrations();
        $this->loadMigrationsFrom([
            '--path' => realpath(__DIR__.'/../database/migrations'),
        ]);

        $this->initEnv();
    }

    public function initEnv()
    {
        $this->user = User::create([
            'name'     => 'Test User',
            'email'    => 'user@example.com',
            'password' => 'secret',
        ]);

        Route::middleware(['middleware' => \Illuminate\Routing\Middleware\SubstituteBindings::class])
            ->prefix('api')
            ->group(function () {
                GetContent::apiRoutes();
            });

        GetContent::editorRoutes();
        GetContent::webRoutes();

        $this->withFactories(__DIR__ . '/../database/factories');
    }
}
