<?php

namespace MilkMedia\GetContent\Test\Features;

use Illuminate\Foundation\Testing\RefreshDatabase;
use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Test\TestCase;

class DocumentScopeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function only_returns_approved_documents()
    {
        factory(Document::class, 2)->create(['status' => Document::STATUS_DRAFT]);
        factory(Document::class, 2)->create(['status' => Document::STATUS_APPROVED]);

        $documents = Document::all();
        $this->assertCount(2, $documents);
    }

    /** @test */
    public function only_returns_published_documents()
    {
        factory(Document::class, 2)->create();
        factory(Document::class, 2)->create([
            'published_at' => now()->subHour(),
            'status'       => Document::STATUS_APPROVED,
        ]);
        factory(Document::class, 2)->create([
            'published_at' => now()->addHour(),
            'status'       => Document::STATUS_APPROVED,
        ]);

        $documents = Document::all();
        $this->assertCount(4, $documents);
    }
}
