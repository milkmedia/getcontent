<?php

namespace MilkMedia\GetContent\Test\Features;

use Illuminate\Foundation\Testing\RefreshDatabase;
use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Test\TestCase;

class DocumentControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function shows_page_title()
    {
        factory(Document::class)->create([
            'name'   => 'My Document',
            'slug'   => 'my-document',
            'status' => 'approved',
        ]);

        $this->get('/my-document')
            ->assertStatus(200)
            ->assertSee('<h1>My Document</h1>');
    }

    /** @test */
    public function renders_document_content_fields()
    {
        factory(Document::class)->create([
            'name'    => 'My Document',
            'slug'    => 'my-document',
            'schema'  => [
                ['type' => 'content', 'model' => 'content1'],
                ['type' => 'media', 'model' => 'media1'],
            ],
            'model'   => [
                'content1' => ['value' => '<h2>This is content</h2><p>nothing special</p>'],
                'media1'   => [
                    'value' => [
                        ['url' => '/storage/files/example.jpg'],
                    ],
                ],
            ],
        ]);

        $this->get('/my-document')
            ->assertStatus(200)
            ->assertSee('<h1>My Document</h1>')
            ->assertSee('<h2>This is content</h2><p>nothing special</p>')
            ->assertSee('src="/_image/storage/files/example.jpg"');
    }

    /** @test */
    public function renders_model_and_schema_like_content()
    {
        factory(Document::class)->create([
            'name'    => 'My Document',
            'slug'    => 'my-document',
            'model'   => [
                'content1' => ['value' => '<h2>This is content</h2><p>nothing special</p>'],
                'media1'   => ['value' => [['url' => '/storage/files/example.jpg']]],
            ],
            'content' => [
                ['type' => 'content', 'model' => 'content1'],
                ['type' => 'media', 'model' => 'media1'],
            ],
        ]);

        $this->get('/my-document')
            ->assertStatus(200)
            ->assertSee('<h1>My Document</h1>')
            ->assertSee('<h2>This is content</h2><p>nothing special</p>')
            ->assertSee('src="/_image/storage/files/example.jpg"');
    }
}
