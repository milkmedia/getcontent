<?php

namespace MilkMedia\GetContent\Test\Features;

use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Group;
use MilkMedia\GetContent\Test\TestCase;

class DocumentSearchTest extends TestCase
{

    /** @test */
    public function search_returns_any_permitted_matching_documents()
    {
        factory(Group::class, 2)->create();
        factory(Group::class)->create(['parent_id' => 1]);

        factory(Document::class, 5)->create([
            'name' => 'Wont see me here',
            'owner_id' => $this->user->id,
            'group_id' => 1,
        ]);

        factory(Document::class)->create([
            'name' => 'This document here',
            'owner_id' => $this->user->id,
            'group_id' => 2,
        ]);

        factory(Document::class)->create([
            'name' => 'Document nested here',
            'owner_id' => $this->user->id,
            'group_id' => 3,
        ]);

        factory(Document::class)->create([
            'name' => 'This document has herons',
            'owner_id' => $this->user->id,
            'group_id' => 3,
        ]);

        factory(Document::class)->create([
            'name' => 'Hearing cheer',
            'owner_id' => $this->user->id,
            'group_id' => 3,
        ]);

        $this->user->settings = ['permissions' => ['groupRootId' => 3]];

        $response = $this->actingAs($this->user)->get('api/documents/search?q=here')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'Document nested here',
            ])
            ->assertJsonFragment([
                'name' => 'Hearing cheer',
            ])
            ->json();

        $this->assertCount(2, $response['data']);
    }

    /** @test */
    public function restrict_search_to_group()
    {
        factory(Group::class, 2)->create();
        factory(Group::class)->create(['parent_id' => 1]);

        factory(Document::class, 5)->create([
            'name' => 'Document in a different group',
            'owner_id' => $this->user->id,
            'group_id' => 1,
        ]);

        factory(Document::class)->create([
            'name' => 'This document here',
            'owner_id' => $this->user->id,
            'group_id' => 2,
        ]);

        factory(Document::class)->create([
            'name' => 'And this document',
            'owner_id' => $this->user->id,
            'group_id' => 2,
        ]);

        factory(Document::class)->create([
            'name' => 'Wont find documents nested deeper',
            'owner_id' => $this->user->id,
            'group_id' => 3,
        ]);

        $this->user->settings = ['permissions' => ['groupRootId' => 2]];

        $response = $this->actingAs($this->user)->get('api/documents/search?q=document&filter[group]=2')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'This document here',
            ])
            ->assertJsonFragment([
                'name' => 'And this document',
            ])
            ->json();

        $this->assertCount(2, $response['data']);
    }

    /** @test */
    public function search_returns_meta_data()
    {
        factory(Document::class, 5)->create(['owner_id' => $this->user->id]);

        $this->withoutExceptionHandling();
        $this->actingAs($this->user)->get('api/documents/search?q=abc')
            ->assertStatus(200)
            ->assertJsonFragment([
                'current_page' => 1
            ]);
    }
}
