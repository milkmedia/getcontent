<?php

namespace MilkMedia\GetContent\Test\Features;

use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Group;
use MilkMedia\GetContent\Template;
use MilkMedia\GetContent\Test\TestCase;

class TemplateTest extends TestCase
{
    /** @test */
    public function user_can_create_templates()
    {
        $this->actingAs($this->user)->post('api/templates', [
            'name'    => 'CTA Block',
            'type'    => 'block',
            'content' => [
                'fields' => [
                    [
                        'type' => 'text',
                    ],
                    [
                        'type' => 'button',
                    ],
                ],
            ],
        ])->assertStatus(201);

        $template = Template::whereRaw("content->'fields'->0->>'type' = 'text'")->first();
        $this->assertEquals(1, $template->id);
    }

    /** @test */
    public function user_can_list_templates()
    {
        factory(Template::class, 5)->create(['type' => 'blocks']);
        factory(Template::class, 5)->create(['type' => 'schema']);

        $this->withoutExceptionHandling();

        $response = $this->actingAs($this->user)->get('api/templates')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'uuid',
                        'name',
                        'content',
                    ],
                ],
            ])
            ->json();

        $this->assertCount(10, $response['data']);
    }

    /** @test */
    public function user_can_get_a_single_template()
    {
        factory(Template::class, 5)->create([
            'type' => 'block',
            'content' => ['fields' => [['type' => 'text']]]
        ]);

        $this->actingAs($this->user)->get('api/templates/1')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'uuid',
                    'name',
                    'content',
                ],
            ])
            ->assertJsonFragment(['content' => ['fields' => [['type' => 'text']]]]);
    }

    /** @test */
    public function user_can_update_a_template()
    {
        factory(Template::class)->create([
            'type' => 'block',
            'content'  => ['fields' => [['type' => 'content'], ['type' => 'image']]],
        ]);

        $this->actingAs($this->user)->put('api/templates/1', [
            'content' => ['fields' => [['type' => 'image']]],
        ])->assertStatus(200);

        $template = Template::find(1);
        $this->assertCount(1, $template->content->fields);
    }

    /** @test */
    public function user_can_delete_template()
    {
        factory(Template::class)->create([
            'type' => 'block',
        ]);

        $this->actingAs($this->user)->delete('api/templates/1')
            ->assertStatus(200);

        $this->assertDatabaseMissing('templates', ['id' => 1]);
    }
}
