<?php

namespace MilkMedia\GetContent\Test\Features;

use Carbon\Carbon;
use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Group;
use MilkMedia\GetContent\Test\TestCase;

class DocumentTest extends TestCase
{
    /** @test */
    public function user_can_create_document()
    {
        $this->actingAs($this->user)->post('api/documents', [
            'name' => 'Hello World',
            'status' => 'approved',
            'content' => [
                'fields' => [
                    [
                        'type' => 'text',
                        'value' => 'Hello World!',
                    ],
                ],
            ],
        ])->assertStatus(201);

        $document = Document::whereRaw("content->'fields'->0->>'value' = 'Hello World!'")->first();
        $this->assertEquals(1, $document->id);
    }

    /** @test */
    public function user_can_list_documents()
    {
        factory(Document::class, 5)->create([
            'owner_id' => $this->user->id,
        ]);

        $response = $this->actingAs($this->user)->get('api/documents')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'name',
                        'created_at',
                        'updated_at',
                    ],
                ],
            ])
            ->json();

        $this->assertCount(5, $response['data']);
    }

    /** @test */
    public function list_documents_in_root_group()
    {
        factory(Document::class, 5)->create([
            'owner_id' => $this->user->id,
        ]);

        factory(Group::class)->create();

        factory(Document::class, 5)->create([
            'owner_id' => $this->user->id,
            'group_id' => 1,
        ]);

        $response = $this->actingAs($this->user)->get('api/documents?filter[group]=')
            ->assertStatus(200)
            ->json();

        $this->assertCount(5, $response['data']);
    }

    /** @test */
    public function list_documents_in_a_subgroup()
    {
        factory(Document::class, 2)->create([
            'owner_id' => $this->user->id,
        ]);

        factory(Group::class)->create();

        factory(Document::class, 5)->create([
            'owner_id' => $this->user->id,
            'group_id' => 1,
        ]);

        $response = $this->actingAs($this->user)->get('api/documents?filter[group]=1')
            ->assertStatus(200)
            ->json();

        $this->assertCount(5, $response['data']);
    }

    /** @test */
    public function user_can_get_a_single_document()
    {
        factory(Document::class, 5)->create([
            'owner_id' => $this->user->id,
        ]);

        $this->actingAs($this->user)->get('api/documents/1')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'description',
                    'model',
                    'schema',
                    'published_at',
                    'created_at',
                    'updated_at',
                ],
            ]);
    }

    /** @test */
    public function user_can_update_a_document()
    {
        factory(Document::class)->create([
            'owner_id' => $this->user->id,
            'content' => [
                'fields' => [
                    [
                        'type' => 'text',
                        'value' => 'Hello World!',
                    ],
                ],
            ],
        ]);

        $this->actingAs($this->user)->put('api/documents/1', [
            'content' => [
                'fields' => [
                    [
                        'type' => 'image',
                        'value' => 'https://example.com/foo.jpg',
                    ],
                ],
            ],
        ])->assertStatus(200);

        $document = Document::whereRaw("content->'fields'->0->>'type' = 'image'")
            ->whereRaw("content->'fields'->0->>'value' = 'https://example.com/foo.jpg'")
            ->first();

        $this->assertEquals(1, $document->id);
    }

    /** @test */
    public function user_can_update_a_draft_document()
    {
        factory(Document::class)->create([
            'owner_id' => $this->user->id,
            'status' => Document::STATUS_DRAFT,
        ]);

        $this->withoutExceptionHandling();
        $this->actingAs($this->user)->put('api/documents/1', [
            'status' => Document::STATUS_APPROVED,
        ])->assertStatus(200);

        $this->assertDatabaseHas('documents', [
            'id' => 1,
            'status' => Document::STATUS_APPROVED,
        ]);
    }

    /** @test */
    public function user_can_delete_document()
    {
        factory(Document::class)->create([
            'owner_id' => $this->user->id,
        ]);

        $this->actingAs($this->user)->delete('api/documents/1')
            ->assertStatus(200);

        $this->assertDatabaseHas('documents', [
            'id' => 1,
            'deleted_at' => Carbon::now(),
        ]);
    }

    /** @test */
    public function user_can_force_delete_document()
    {
        factory(Document::class)->create([
            'owner_id' => $this->user->id,
        ]);

        $this->actingAs($this->user)->delete('api/documents/1?force')
            ->assertStatus(200);

        $this->assertDatabaseMissing('documents', ['id' => 1]);
    }

    /** @test */
    public function document_can_be_added_to_group()
    {
        factory(Group::class)->create();

        $this->actingAs($this->user)->post('api/documents', [
            'name' => 'New Page',
            'group_id' => 1,
            'content' => ['content' => 'test'],
        ])->assertStatus(201);

        $this->assertDatabaseHas('documents', ['id' => 1, 'group_id' => 1]);
    }

    /** @test */
    public function cant_add_document_to_non_existent_group()
    {
        $this->actingAs($this->user)->post('api/documents', [
            'name' => 'New Page',
            'group_id' => 1,
            'content' => ['content' => 'test'],
        ])->assertStatus(302);

        $this->assertDatabaseMissing('documents', ['id' => 1, 'group_id' => 1]);
    }

    /** @test */
    public function user_can_move_document_group()
    {
        factory(Group::class, 2)->create();

        factory(Document::class)->create([
            'owner_id' => $this->user->id,
            'group_id' => 1,
        ]);

        $this->actingAs($this->user)->put('api/documents/1', [
            'group_id' => 2,
        ])->assertStatus(200);

        $this->assertDatabaseHas('documents', ['id' => 1, 'group_id' => 2]);
    }

    /** @test */
    public function cannot_move_document_to_non_existent_group()
    {
        factory(Group::class)->create();

        factory(Document::class)->create([
            'owner_id' => $this->user->id,
            'group_id' => 1,
        ]);

        $this->actingAs($this->user)->put('api/documents/1', [
            'group_id' => 2,
        ])->assertStatus(302);

        $this->assertDatabaseHas('documents', ['id' => 1, 'group_id' => 1]);
    }

    /** @test */
    public function document_can_link_to_other_documents()
    {
        factory(Document::class, 2)->create([
            'owner_id' => $this->user->id,
            'status' => Document::STATUS_APPROVED,
        ]);

        $this->withoutExceptionHandling()->actingAs($this->user)->post('api/documents', [
            'name' => 'New Doc with links',
            'documents' => [['id' => 1], ['id' => 2]],
            'status' => Document::STATUS_APPROVED,
        ])
            ->assertStatus(201);

        $linkedDocs = Document::whereId(3)->first()->documents;

        $this->assertCount(2, $linkedDocs);
    }

    /** @test */
    public function document_can_included_linked_documents()
    {
        $doc = factory(Document::class)->create([
            'owner_id' => $this->user->id,
            'status' => Document::STATUS_APPROVED,
        ]);

        factory(Document::class)->create([
            'owner_id' => $this->user->id,
            'status' => Document::STATUS_APPROVED,
        ])->documents()->attach($doc);

        $response = $this->actingAs($this->user)->get('api/documents/2?include=documents,group')
            ->assertStatus(200)
            ->json();

        $this->assertCount(1, $response['data']['documents']);
    }

    /** @test */
    public function document_can_see_its_references()
    {
        $docs = factory(Document::class, 2)->create([
            'owner_id' => $this->user->id,
            'status' => Document::STATUS_APPROVED,
        ]);

        factory(Document::class)->create([
            'owner_id' => $this->user->id,
            'status' => Document::STATUS_APPROVED,
        ])->documents()->attach($docs);

        $this->assertEquals(3, Document::find(1)->referencedBy[0]->id);
        $this->assertEquals(3, Document::find(2)->referencedBy[0]->id);
        $this->assertCount(0, Document::find(3)->referencedBy);
    }

    /** @test */
    public function document_links_can_be_updated()
    {
        factory(Document::class, 3)->create([
            'owner_id' => $this->user->id,
            'status' => Document::STATUS_APPROVED,
        ]);

        $document = Document::find(3);
        $document->documents()->sync([1, 2]);

        $this->actingAs($this->user)->put('api/documents/3', [
            'documents' => [['id' => 1]],
        ])
            ->assertStatus(200);

        $linkedDocs = $document->fresh()->documents;

        $this->assertCount(1, $linkedDocs);
    }

    /** @test */
    public function document_links_can_be_removed()
    {
        factory(Document::class, 3)->create([
            'owner_id' => $this->user->id,
            'status' => Document::STATUS_APPROVED,
        ]);

        Document::find(3)->documents()->sync([1, 2]);

        $this->actingAs($this->user)->put('api/documents/3', [
            'documents' => null,
        ])
            ->assertStatus(200);

        $linkedDocs = Document::find(3)->first()->documents;

        $this->assertCount(0, $linkedDocs);
    }
}
