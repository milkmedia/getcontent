<?php

namespace MilkMedia\GetContent\Test\Features;

use Illuminate\Http\Testing\File;
use Illuminate\Support\Facades\Storage;
use MilkMedia\GetContent\Test\TestCase;

class FileTest extends TestCase
{
    /** @test */
    public function user_can_upload_files()
    {
        Storage::fake();

        $this->actingAs($this->user)->post('api/files', [
            'file' => File::image('this example.jpg'),
        ])
            ->assertStatus(201)
            ->assertJson(['data' => ['path' => '/this-example.jpg']]);

        Storage::disk()->assertExists('public/files/this-example.jpg');
    }

    /** @test */
    function users_can_list_files()
    {
        Storage::fake();
        Storage::putFileAs('public/files', File::create('example 1.jpg', 5000), 'example-1.jpg');
        Storage::putFileAs('public/files', File::create('example 2.jpg', 82000), 'example-2.jpg');

        $this->actingAs($this->user)->get('api/files')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'files' => [
                        [
                            'name' => 'example-1.jpg',
                            'path' => '/example-1.jpg',
                            'url'  => '/storage/files/example-1.jpg',
                        ],
                        [
                            'name' => 'example-2.jpg',
                            'path' => '/example-2.jpg',
                            'url'  => '/storage/files/example-2.jpg',
                        ],
                    ],
                ],
            ]);
    }

    /** @test */
    public function listing_includes_directories()
    {
        Storage::fake();
        Storage::putFileAs('public/files', File::create('example 1.jpg', 5000), 'example-1.jpg');
        Storage::putFileAs('public/files/example_folder', File::create('example 2.jpg', 82000), 'example-2.jpg');

        $this->actingAs($this->user)->get('api/files')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'files'       => [
                        [
                            'name' => 'example-1.jpg',
                            'path' => '/example-1.jpg',
                            'url'  => '/storage/files/example-1.jpg',
                        ],
                    ],
                    'directories' => [
                        [
                            'name' => 'example_folder',
                            'path' => '/example_folder',
                        ],
                    ],
                ],
            ]);
    }

    /** @test */
    public function can_list_subdirectories()
    {
        Storage::fake();
        Storage::putFileAs('public/files', File::create('example 1.jpg', 5000), 'example-1.jpg');
        Storage::putFileAs('public/files/example_folder', File::create('example 2.jpg', 82000), 'example-2.jpg');

        $this->actingAs($this->user)->get('api/files/example_folder')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'files'       => [
                        [
                            'name' => 'example-2.jpg',
                            'path' => '/example_folder/example-2.jpg',
                            'url'  => '/storage/files/example_folder/example-2.jpg',
                        ],
                    ],
                    'directories' => [],
                ],
            ]);
    }

    /** @test */
    public function file_can_be_deleted()
    {
        Storage::fake();
        Storage::putFileAs('public/files/folder', File::create('example.jpg', 82000), 'example.jpg');

        $this->actingAs($this->user)->delete('api/files/folder/example.jpg')
            ->assertStatus(202);

        $this->assertFalse(Storage::exists('public/files/folder/example.jpg'));
    }

    /** @test */
    public function directory_can_be_created()
    {
        Storage::fake();

        $this->actingAs($this->user)->post('api/files', ['directory' => 'test'])
            ->assertStatus(201)
            ->assertJson(['data' => ['url' => '/storage/files/test']]);

        $this->assertTrue(Storage::exists('public/files/test'));
    }

    /** @test */
    public function directory_can_be_deleted()
    {
        Storage::fake();
        Storage::makeDirectory('public/files/test');
        $this->assertTrue(Storage::exists('public/files/test'));

        $this->actingAs($this->user)->delete('api/files/test')
            ->assertStatus(202);

        $this->assertFalse(Storage::exists('public/files/test'));
    }

    /** @test */
    public function files_can_be_uploaded_to_subdirectory()
    {
        Storage::fake();

        $this->actingAs($this->user)->post('api/files/example', [
            'file' => File::image('this example.jpg'),
        ])
            ->assertStatus(201)
            ->assertJson(['data' => ['path' => '/example/this-example.jpg']]);

        Storage::disk()->assertExists('public/files/example/this-example.jpg');
    }

    /** @test */
    public function search_files_in_directories()
    {
        Storage::fake();
        Storage::putFileAs('public/files', File::create('example-1.jpg'), 'example-1.jpg');
        Storage::putFileAs('public/files', File::create('another file.pdf'), 'another file.pdf');
        Storage::putFileAs('public/files/folder', File::create('example.jpg'), 'example.jpg');
        Storage::putFileAs('public/files/folder/deep', File::create('buried.pdf'), 'buried.pdf');
        Storage::putFileAs('public/files/folder/deep', File::create('buried-example.pdf'), 'buried-example.pdf');

        $this->withExceptionHandling();
        $this->actingAs($this->user)->get('api/files?q=example')
            ->assertStatus(200)
            ->assertJsonFragment(['url'  => '/storage/files/example-1.jpg'])
            ->assertJsonFragment(['url'  => '/storage/files/folder/deep/buried-example.pdf'])
            ->assertJsonMissing(['url'  => '/storage/files/folder/deep/buried.pdf'])
            ->assertJsonMissing(['url'  => '/storage/files/another file.pdf']);
    }
}
