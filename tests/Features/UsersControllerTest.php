<?php

namespace MilkMedia\GetContent\Test\Features;

use MilkMedia\GetContent\Test\TestCase;

class UsersControllerTest extends TestCase
{
    /** @test */
    public function it_returns_the_user_authorised_by_the_request()
    {
        $this->actingAs($this->user)->get('api/users/me')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'name' => 'Test User',
                    'email' => 'user@example.com'
                ]
            ]);
    }
}
