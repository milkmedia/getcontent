<?php

namespace MilkMedia\GetContent\Test\Features;

use Illuminate\Foundation\Testing\RefreshDatabase;
use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Group;
use MilkMedia\GetContent\Test\TestCase;

class DocumentRootTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function only_documents_in_users_root_are_returned()
    {
        factory(Document::class, 1)->create();
        factory(Group::class, 2)->create();
        factory(Document::class, 2)->create(['group_id' => 1]);
        factory(Document::class, 2)->create(['group_id' => 2, 'name' => 'Group 2 Documents']);

        $this->user->settings = ['permissions' => ['groupRootId' => 2]];

        $resp = $this->actingAs($this->user)
            ->get('api/documents')
            ->assertJsonCount(2, 'data')
            ->json();

        $this->assertArrayHasKey('data', $resp);
    }

    /** @test */
    public function returns_unauthorised_requesting_documents_from_groups_out_of_root()
    {
        factory(Document::class, 1)->create();
        factory(Group::class, 2)->create();
        factory(Document::class, 2)->create(['group_id' => 1]);
        factory(Document::class, 2)->create(['group_id' => 2, 'name' => 'Group 2 Documents']);

        $this->user->settings = ['permissions' => ['groupRootId' => 2]];

        $resp = $this->actingAs($this->user)
            ->get('api/documents?filter[group]=1')
            ->assertStatus(403);
    }

    /** @test */
    public function only_show_groups_in_root_group()
    {
        factory(Group::class, 2)->create();
        factory(Group::class, 2)->create(['parent_id' => 1]);
        factory(Group::class)->create(['parent_id' => 2, 'name' => 'Child Group']);

        $this->user->settings = ['permissions' => ['groupRootId' => 2]];

        $this->actingAs($this->user)
            ->get('api/groups')
            ->assertJsonCount(1, 'data')
            ->assertJsonFragment(['name' => 'Child Group'])
            ->json();
    }

    /** @test */
    public function listing_child_groups_out_of_root_returns_unauthorised()
    {
        factory(Group::class, 2)->create();
        factory(Group::class, 2)->create(['parent_id' => 1]);

        $this->user->settings = ['permissions' => ['groupRootId' => 2]];

        $resp = $this->actingAs($this->user)
            ->get('api/groups?filter[parent]=1')
            ->assertStatus(403);
    }
}
