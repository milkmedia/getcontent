<?php

namespace MilkMedia\GetContent\Test\Features;

use Illuminate\Foundation\Testing\RefreshDatabase;
use MilkMedia\GetContent\Document;
use MilkMedia\GetContent\Test\TestCase;

class DocumentFilterTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function search_by_name()
    {
        factory(Document::class, 5)->create([
            'owner_id' => $this->user->id,
        ]);

        factory(Document::class)->create([
            'name'     => 'This document here',
            'owner_id' => $this->user->id,
        ]);

        $response = $this->actingAs($this->user)->get('api/documents?filter[name]=here')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'This document here',
            ])
            ->json();

        $this->assertCount(1, $response['data']);
    }

}
