<?php

namespace MilkMedia\GetContent\Test\Features;

use Illuminate\Http\Testing\File;
use Illuminate\Support\Facades\Storage;
use MilkMedia\GetContent\Test\TestCase;

class FileRestrictionTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake();
        Storage::putFileAs('public/files', File::create('example 1.jpg', 5000), 'example-1.jpg');
        Storage::putFileAs('public/files', File::create('example 2.jpg', 82000), 'example-2.jpg');
        Storage::putFileAs('public/files/users', File::create('example 3.jpg', 82000), 'example-3.jpg');
        Storage::putFileAs('public/files/users', File::create('example 4.jpg', 82000), 'example-4.jpg');
        Storage::putFileAs('public/files/users/sub-directory', File::create('example 5.jpg', 82000), 'example-5.jpg');
    }

    /** @test */
    public function returns_files_from_users_root_directory()
    {
        $this->user->settings = ['permissions' => ['filesRootDirectory' => 'users']];

        $this->withoutExceptionHandling()->actingAs($this->user)->get('api/files')
            ->assertStatus(200)
            ->assertJsonMissing(['name' => 'example-1.jpg'])
            ->assertJsonFragment(['name' => 'example-3.jpg'])
            ->assertJsonFragment(['name' => 'example-4.jpg']);
    }

    /** @test */
    public function cant_jump_out_root_directory()
    {
        $this->user->settings = ['permissions' => ['filesRootDirectory' => 'users']];

        $this->actingAs($this->user)->get('api/files/../')
            ->assertStatus(200)
            ->assertJsonMissing(['name' => 'example-1.jpg'])
            ->assertJsonFragment(['name' => 'example-3.jpg'])
            ->assertJsonFragment(['name' => 'example-4.jpg']);
    }

    /** @test */
    public function directories_are_created_under_user_root()
    {
        $this->user->settings = ['permissions' => ['filesRootDirectory' => 'users']];

        $this->withoutExceptionHandling()->actingAs($this->user)->post('api/files', ['directory' => 'test'])
            ->assertStatus(201)
            ->assertJson(['data' => ['url' => '/storage/files/users/test']]);

        $this->assertTrue(Storage::exists('public/files/users/test'));
        $this->assertFalse(Storage::exists('public/files/test'));
    }

    /** @test */
    public function user_can_only_upload_to_permitted_directory()
    {
        $this->user->settings = ['permissions' => ['filesRootDirectory' => 'users']];

        $this->actingAs($this->user)->post('api/files', [
            'file' => File::image('this example.jpg'),
        ])
            ->assertStatus(201)
            ->assertJson(['data' => ['path' => '/this-example.jpg']]);

        Storage::disk()->assertExists('public/files/users/this-example.jpg');
    }

    /** @test */
    public function user_cannot_deleted_above_permitted_directory()
    {
        $this->user->settings = ['permissions' => ['filesRootDirectory' => 'users']];
        Storage::putFileAs('public/files/folder', File::create('example.jpg', 82000), 'example.jpg');

        $this->actingAs($this->user)->delete('api/files/folder/example.jpg')
            ->assertStatus(404);

        $this->assertTrue(Storage::exists('public/files/folder/example.jpg'));
    }
}
