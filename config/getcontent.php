<?php

return [
    'storage_path' => env('GETCONTENT_STORAGE_PATH', 'public/files'),
    'path' => env('GETCONTENT_PATH', 'editor'),
    'models' => [
        'owner' => '\App\User',
    ]
];
