<section>
    @foreach(data_get($field, 'model.value') as $image)
        <figure>
            <img src="/_image{{data_get($image, 'url')}}" alt="{{data_get($image, 'alt')}}"
                 class="{{data_get($field, 'css_class')}} {{data_get($field, 'options.image_size')}}">
            @if(Arr::has($field, 'caption'))
                <figcaption>
                    {{data_get($field, 'caption')}}
                </figcaption>
            @endif
        </figure>
    @endforeach
</section>
