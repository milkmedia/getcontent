<section @if($cssClass = data_get($template, 'css_class')) class="{{$cssClass}}" @endif>
    @each('GetContent::field', $fields, 'field')
</section>
