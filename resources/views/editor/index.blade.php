@extends('GetContent::editor.layouts.default')

@push('styles')
    <link rel="stylesheet" href="{{asset('vendor/milkmedia/getcontent/css/app.css?'.\MilkMedia\GetContent\GetContent::$VERSION)}}">
@endpush

@push('scripts')
    <script>
        window.config = {
            api_token: "{{Auth::user()->api_token}}",
            csrf_token: "{{csrf_token()}}"
        };
    </script>
    <script src="{{asset('vendor/milkmedia/getcontent/js/manifest.js?'.\MilkMedia\GetContent\GetContent::$VERSION)}}"></script>
    <script src="{{asset('vendor/milkmedia/getcontent/js/vendor.js?'.\MilkMedia\GetContent\GetContent::$VERSION)}}"></script>
    <script src="{{asset('vendor/milkmedia/getcontent/js/app.js?'.\MilkMedia\GetContent\GetContent::$VERSION)}}"></script>
@endpush

@section('content')
    <div id="getcontent">
        <getcontent-app></getcontent-app>
    </div>
@endsection
