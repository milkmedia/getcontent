<h1>{{$document->name}}</h1>
@if ($document->fields)
<article>
    @each('GetContent::field', $document->fields, 'field')
</article>
@endif
