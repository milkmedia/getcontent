let mix = require("laravel-mix");
let tailwindcss = require("tailwindcss");
require("laravel-mix-purgecss");

mix
  .js("src/frontend/js/app.js", "js")
  .sass("src/frontend/sass/app.scss", "css")
  .extract()
  // .version()
  .sourceMaps(false, "eval-source-map")
  .options({
    processCssUrls: false,
    postCss: [tailwindcss("./tailwind.js")]
  })
  /*.purgeCss({
    folders: ["resources", "src"],
    whitelistPatterns: []
  })*/

  .webpackConfig({
    output: {
      path: path.resolve(__dirname, "public"),
      publicPath: "/vendor/milkmedia/getcontent/"
    }
  });
