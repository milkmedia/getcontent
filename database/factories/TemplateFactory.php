<?php

use Faker\Generator as Faker;
use MilkMedia\GetContent\Template;

$factory->define(Template::class, function (Faker $faker) {
    return [
        'name'    => $faker->word,
        'content' => [
            'fields' => [
                [
                    'name'     => 'header',
                    'type'     => 'text',
                    'cssClass' => 'text-xl',
                ],
                [
                    'name'     => 'text',
                    'type'     => 'text',
                    'cssClass' => 'text-lg',
                ],
                [
                    'type' => 'replicator',
                    'sets' => [
                        ['type' => 'button'],
                    ],
                ],
            ],
        ],
    ];
});
