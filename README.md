# GetContent CMS

GetContent CMS is a [Laravel](https://laravel.com) package that provides an
unstructured, headless content API to use within your own Laravel project.
It comes with an user interface to manage content and media out of the box.

## Install

### Migrations

… Adds a `settings` `json` column to the `users` table.

### Routes

`routes/api.php`
```php
Route::middleware('auth:api')->group(function () {
    GetContent::apiRoutes();
});
```

`routes/web.php`
```php
Route::middleware('auth')->group(function () {
    GetContent::editorRoutes();
});

GetContent::webRoutes();
```

### Users, Authentication and Authorisation

GetContent has it's own `User` model that you should extend

## Development

### Testing

Spin up postgres instance on port `5532` and run `phpunit`.

```bash
docker-compose -f tests/docker-compose.yml up -d
./vendor/phpunit/phpunit/phpunit
```

Later…

```bash
docker-compose -f tests/docker-compose.yml down
```

### Test Gitlab CI

```bash
gitlab-runner exec docker phpunit
```

## Documents

Content is stored within a Document. A Document has a `model` and a `schema`.

### Model

The model is a simple key:value object containing a `value` and any other
optional properties you wish.

```json
{
  "content1": {
    "value": "<p>This is some content</p>"
  }
}
```

### Schema

The schema is an array of objects that defines the kind of data stored in the
model and how the entry form should be laid out. A schema is fluid by default
so you can add any number or fields in any arrangement. However it is possible
to created a fixed schema so a given Document has an enforced schema.

```json
[
  {
    "type": "content",
    "model": "content1",
    "label": "Body Content",
    "instructions": "Enter some content here.",
    "options": {}
  }
]
```

### Templates

Templates are field type that groups together multiple other fields.

```json
[
  {
    "type": "template",
    "model": "content1",
    "template": {
      "fields": [
        {
          "type": "content",
          "model": "content1"
        },
        {
          "type": "media",
          "model": "media1"
        }
      ]
    }
  }
]
```

Or you can use a globally defined template

```json
[
  {
    "type": "template",
    "model": "content1",
    "template": "contentWithImage"
  }
]
```

## Replicating field sets

A replicating field set allows you to add multiple instances of a set of fields.

```json
[
  {
    "type": "template",
    "model": "socialLinks",
    "template": {
      "replicator": true,
      "sets": [
        {
          "display": "Social Link",
          "fields": {
            "url": {
              "type": "text",
              "model": "url",
              "placeholder": "Social profile URL"
            }
          }
        }
      ]
    }
  }
]
```

## User Permissions

Users can be restricted to specific areas of content.

### Document and Files

```json
{
  "permissions": {
    "filesRootDirectory": "subdirectory",
    "groupRootId": 2
  }
}
```
